﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Blazor.Shop.Management.Services;

namespace Blazor.Shop.Management.ViewModels
{
	public class UserViewModel : BaseViewModel
	{
		public UserViewModel()
		{
			this.Roles = new CommonService().GetRoles().Result;
		}

		[Required]
		public string FullName { get; set; } = string.Empty;
		public string Email { get; set; } = string.Empty;
		[Required]
		public string PhoneNumber { get; set; } = string.Empty;
		[Required]
		public string Address { get; set; } = string.Empty;
		[Required]
		public string Role { get; set; } = string.Empty;
		public IList<string> Roles { get; }
		public string Token { get; set; }
	}
}
