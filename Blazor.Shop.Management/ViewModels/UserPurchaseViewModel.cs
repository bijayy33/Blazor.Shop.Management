﻿namespace Blazor.Shop.Management.ViewModels
{
	public class UserPurchaseViewModel
	{
		public string FullName { get; set; }
		public string Emails { get; set; }
		public string Phones { get; set; }
		public string Address { get; set; }
		public int Quantity { get; set; }
		public decimal TotalAmount { get; set; } //Total
		public decimal TotalAmountPaid { get; set; } //Total Paid
		public decimal TotalPendingAmount { get; set; }
		public decimal TotalDiscountAmount { get; set; }
		public bool OperationCancelled { get; set; }
	}
}
