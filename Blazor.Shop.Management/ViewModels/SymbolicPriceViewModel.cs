﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Blazor.Shop.Management.ViewModels
{
	public class SymbolicPriceViewModel
	{
		public string Id { get; set; }

		[Required]
		public string FiscalYear { get; set; } //FY 2077 or 2077 or All
		[Required]
		public char Alphabet { get; set; }

		[Required]
		public int Value { get; set; }
		public bool IsDeleted { get; set; }
	}
}
