﻿using System;

using Blazor.Shop.Management.Common.Enums;

namespace Blazor.Shop.Management.ViewModels
{
	public class BaseViewModel
	{
		public string Id { get; set; }
		public ActionType ActionType { get; set; } = ActionType.None;
		public bool IsDeleted { get; set; }
		public string CreatedById { get; set; }
		public DateTimeOffset? CreationDate { get; set; }
		public string ChangedById { get; set; }
	}
}
