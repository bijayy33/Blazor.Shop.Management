﻿using System;
using System.ComponentModel.DataAnnotations;

using Blazor.Shop.Management.Common.Enums;

namespace Blazor.Shop.Management.ViewModels
{
	public class ProductViewModel : BaseViewModel
	{
		[Required]
		public string Code { get; set; }

		[Required]
		public string Description { get; set; }
		public string Size { get; set; }
		public decimal Mrp { get; set; } //Symbolic MRP or direct value

		[Display(Name = "Cost Price")]
		public decimal CostPrice { get; set; } //Symbolic Cost Price or direct value} //Symbolic Cost Price or direct value

		[Display(Name = "Selling Price")]
		public decimal SellingPrice { get; set; } //Symbolic SP or direct value

		[Required]
		public int AvailableQuantity { get; set; } = 0;

		[Required]
		public QuantityType QuantityType { get; set; } = Common.Enums.QuantityType.PCS;
		public int QuantityTypeValue { get; set; } = (int)Common.Enums.QuantityType.PCS;
		public string[] QuantityTypes => Enum.GetNames(typeof(QuantityType));
	}
}
