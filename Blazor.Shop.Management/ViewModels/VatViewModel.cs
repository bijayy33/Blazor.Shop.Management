﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Blazor.Shop.Management.ViewModels
{
	public class VatViewModel
	{
		public virtual int InvoiceNumber { get; set; }
		[Required]
		public virtual decimal VatPercentage { get; set; } = 13; //Default 13%
		[Required]
		public virtual decimal TotalTaxableAmount { get; set; }
		[Required]
		public virtual decimal TotalVatAmount { get; set; }
		[Required]
		public virtual decimal TotalAmount { get; set; }
		public DateTimeOffset? CreationDate { get; set; }
		public virtual List<ProductVatViewModel> ProductVats { get; set; } = new List<ProductVatViewModel>();
	}
}
