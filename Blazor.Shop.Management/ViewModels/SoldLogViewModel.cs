﻿using System;

namespace Blazor.Shop.Management.ViewModels
{
	public class SoldLogViewModel
	{
		public string Id { get; set; }
		public virtual decimal TotalAmount { get; set; } // Gross Amount i.e. 2 * Selling Price
		public virtual decimal TotalPaidAmount { get; set; } //Amount Paid by customer.
		public virtual decimal TotalDiscountAmount { get; set; } //Discount given to customer.
		public virtual decimal TotalPendingAmount { get; set; } //Pending Amount to take from customer.
		public virtual DateTimeOffset? CreationDate { get; set; }
		public string SoldId { get; set; } // SoldProductId.
		public SoldViewModel Sold { get; set; } // Sold Product.
	}
}
