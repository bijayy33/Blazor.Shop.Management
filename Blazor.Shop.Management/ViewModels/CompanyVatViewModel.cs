﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Blazor.Shop.Management.ViewModels
{
	public class CompanyVatViewModel
	{
		public string Id { get; set; }
		[Required]
		public string BillNumber { get; set; }
		[Required]
		public decimal VatPercentage { get; set; } = 13;
		[Required]
		public decimal PurchasedAmount { get; set; }
		[Required]
		public decimal TaxAmount { get; set; }
		[Required]
		public decimal AmountWithoutTax { get; set; }
		public string CompanyId { get; set; }
		[Required]
		public CompanyViewModel Company { get; set; } = new CompanyViewModel();
		public DateTimeOffset? CreationDate { get; set; }
		public DateTimeOffset? ChangedDate { get; set; }
	}
}
