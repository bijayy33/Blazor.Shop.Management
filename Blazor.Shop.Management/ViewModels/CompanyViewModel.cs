﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Blazor.Shop.Management.Common.Enums;

namespace Blazor.Shop.Management.ViewModels
{
	public class CompanyViewModel
	{
		public virtual string Id { get; set; }
		[Required]
		public virtual string Name { get; set; }
		[Required]
		public virtual string Description { get; set; } //John Deer, Sonalika, Gulf, etc
		[Required]
		public virtual string TaxTypeNumber { get; set; } //VAT or PAN
		public virtual string Emails { get; set; }
		[Required]
		public virtual string Phones { get; set; }
		[Required]
		public virtual string Address { get; set; }
		public virtual TaxType TaxType { get; set; } = TaxType.PAN;
		public virtual string[] TaxTypes => Enum.GetNames(typeof(TaxType));
		public virtual CompanyType Type { get; set; } = CompanyType.Distributer;
		public virtual string[] CompanyTypes => Enum.GetNames(typeof(CompanyType));
		public List<CompanyVatViewModel> CompanyVats { get; set; }
		public DateTimeOffset? CreationDate { get; set; }
		public DateTimeOffset? ChangedDate { get; set; }
	}
}
