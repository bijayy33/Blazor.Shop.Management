﻿using System.Collections.Generic;
using System.Net;

using Microsoft.AspNetCore.Identity;

namespace Blazor.Shop.Management.ViewModels
{
	public class ResponseViewModel<T>
	{
		public ResponseViewModel()
		{
			Errors = new List<IdentityError>();
		}

		public T Data { get; set; }
		public HttpStatusCode StatusCode { get; set; }
		public IEnumerable<IdentityError> Errors { get; set; }
		public bool IsSuccess { get; set; }
	}
}
