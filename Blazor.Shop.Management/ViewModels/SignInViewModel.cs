﻿using System.ComponentModel.DataAnnotations;

namespace Blazor.Shop.Management.ViewModels
{
	public class SignInViewModel : BaseViewModel
	{
		[Required]
		[Display(Name = "Mobile Number Or Email")]
		public string UserName { get; set; } = string.Empty;

		[Required]
		public string Password { get; set; } = string.Empty;
	}
}
