﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Blazor.Shop.Management.Common.Enums;

namespace Blazor.Shop.Management.ViewModels
{
	public class ProductVatViewModel
	{
		public string ProductId { get; set; } //Internal Purpose
		public ProductViewModel Product { get; set; } = new ProductViewModel(); //Internal Purpose
		[Required]
		public int Quantity { get; set; } = 1; //Product Quanties
		public QuantityType QuantityType { get; set; } = QuantityType.PCS;
		public IEnumerable<string> QuantityTypes => Enum.GetNames(typeof(QuantityType));
		[Required]
		public decimal Rate { get; set; }
		[Required]
		public decimal VatPercentage { get; set; } = 13;
		public decimal DiscountAmount { get; set; } = 0;
		[Required]
		public decimal TaxableAmount { get; set; }
		[Required]
		public decimal TaxAmount { get; set; }
		[Required]
		public decimal Amount { get; set; }
		public VatViewModel Vat { get; set; }
	}
}
