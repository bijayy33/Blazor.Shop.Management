﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Blazor.Shop.Management.Common.Enums;

namespace Blazor.Shop.Management.ViewModels
{
	public class SoldProductViewModel
	{
		public string Id { get; set; }
		[Required]
		public int AvailableQuantity { get; set; } // Quantity Available when sold 2.
		[Required]
		public int SoldQuantity { get; set; } // Quantity Sold 2.
		public QuantityType QuantityType { get; set; } = QuantityType.PCS; // Quantity Type.
		public string[] QuantityTypes => Enum.GetNames(typeof(QuantityType)); // Quantity Type.
		[Required]
		public decimal Rate { get; set; }
		[Required]
		public decimal Amount { get; set; }
		public decimal DiscountAmount { get; set; }
		public string SoldId { get; set; } // SoldProductId.
		public SoldViewModel Sold { get; set; } // Sold Product.
		public string ProductId { get; set; } // Sold Product.
		public ProductViewModel Product { get; set; } = new ProductViewModel(); // Sold Product.
	}
}
