﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Blazor.Shop.Management.ViewModels
{
	public class SoldViewModel
	{
		public virtual string Id { get; set; } // Id.
		public virtual string SellerId { get; set; } // Seller Id.
		public virtual string BuyerId { get; set; } // Seller Id.
		[Required]
		public virtual decimal TotalAmount { get; set; } // Gross Amount i.e. 2 * Selling Price
		[Required]
		public virtual decimal TotalPaidAmount { get; set; } //Amount Paid by customer.
		public virtual decimal TotalDiscountAmount { get; set; } //Discount given to customer.
		[Required]
		public virtual decimal TotalPendingAmount { get; set; } //Pending Amount to take from customer.
		public virtual List<SoldProductViewModel> SoldProducts { get; set; } = new List<SoldProductViewModel>();
		public virtual List<SoldLogViewModel> SoldLogs { get; set; } = new List<SoldLogViewModel>();
		public virtual UserViewModel Buyer { get; set; }
		public virtual UserViewModel Seller { get; set; }
		public virtual DateTimeOffset? CreationDate { get; set; }
	}
}
