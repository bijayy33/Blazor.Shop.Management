﻿namespace Blazor.Shop.Management.Common.Constants
{
	public class DefaultRole
	{
		//Default Roles Ids
		public const string SuperAdminId = "CFD05CE8-DEE4-443E-9951-F43926F4546B";
		public const string SellerId = "94FFDD19-A434-4DF1-AB07-8D5F89B774B2";
		public const string BuyerId = "86EB3F7D-ACD4-4A22-A798-A903B10EE17A";

		//Default Roles
		public const string SuperAdmin = nameof(SuperAdmin);
		public const string Seller = nameof(Seller);
		public const string Buyer = nameof(Buyer);
	}
}
