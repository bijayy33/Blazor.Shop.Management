﻿namespace Blazor.Shop.Management.Common.Constants
{
	public class DefaultUser
	{
		//Prevent Duplicate entry by predefining the ids
		public const string SuperAdminId = "1A71FB8E-BB35-432B-B614-A76195F300AA";

		//UserName and Email
		public const string SuperAdminEmail = "superadmin@shop.com";
		public const long SuperAdminPhoneNumber = 9876543210;
		public const string Password = "IAmTheLegend!@12";
	}
}
