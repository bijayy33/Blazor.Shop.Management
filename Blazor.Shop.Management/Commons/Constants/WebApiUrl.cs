﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blazor.Shop.Management.Common.Constants
{
	public class WebApiUrl
	{
		//Common
		public const string ApiUrl = "/api";
		public const string Param = "{param}";

		//User
		public const string UserLoginUrl = "api/user/login";
		public const string UserLogoutUrl = "api/user/logout";
		public const string UserListUrl = "api/user/all";
		public const string AddUserUrl = "api/user/add";
		public const string UpdateUserByParamUrl = "api/user/update/{param}";
		public const string DeleteUserUrl = "api/user/{param}";

		//product
		/// <summary>
		/// For get, update and delete.
		/// </summary>
		public const string ProductByParamUrl = "api/shop/product/{param}";
		public const string ProductListUrl = "api/shop/product/all";
		public const string AddProductUrl = "api/shop/product/add";
		public const string AddProductListUrl = "api/shop/product/multiple";

		//Sold Product
		public const string AllSoldListUrl = "api/shop/sold/all";
		public const string AddSoldUrl = "api/shop/sold";
		public const string AddSoldListUrl = "api/shop/sold/multiple";
		public const string SoldByParamUrl = "api/shop/sold/{param}";
		public const string SoldProductByParamUrl = "api/shop/sold/product/{param}";

		//Sold Logs
		public const string SoldLogsByParamUrl = "api/shop/sold/logs/{param}";

		//Vat
		public const string VatListUrl = "api/shop/product/vat/all";
		public const string VatByParamUrl = "api/shop/product/vat/{param}";
		public const string AddVatUrl = "api/shop/product/vat";

		//User Product Report

		//Symbolic Price

		/// <summary>
		/// For get, update and delete.
		/// </summary>
		public const string SymbolicPriceByParamUrl = "api/shop/symbolic-price/{param}";
		public const string SymbolicPriceListUrl = "api/shop/symbolic-price/all";
		public const string AddSymbolicPriceUrl = "api/shop/symbolic-price";

		/// <summary>
		/// For get, update and delete.
		/// </summary>
		public const string CompanyByParamUrl = "api/shop/company/{param}";
		public const string CompanyListUrl = "api/shop/company/all";
		public const string AddCompanyUrl = "api/shop/company";

		/// <summary>
		/// For get, update and delete.
		/// </summary>
		public const string CompanyVatByParamUrl = "api/shop/company/vat/{param}";
		public const string CompanyVatListUrl = "api/shop/company/vat/all";
		public const string AddCompanyVatUrl = "api/shop/company/vat";
	}
}
