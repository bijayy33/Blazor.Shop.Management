﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blazor.Shop.Management.Common.Constants
{
	public class CascadingParameterName
	{
		public const string SideMenu = nameof(SideMenu);
		public const string NormalHourGlass = nameof(NormalHourGlass);
	}
}
