﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blazor.Shop.Management.Common.Constants
{
	public class LocalUrl
	{
		public const string RedirectString = "/redirect";
		public const string Param = "{param}";

		public const string LogListUrl = "/log/all";

		public const string SymbolicPriceListUrl = "/symbolic-price/all";
		public const string SymbolicPriceByParamUrl = "/symbolic-price/{param}";

		public const string ProductByParamUrl = "/product/all/{param}";
		public const string ProductListUrl = "/product/all";
		public const string AddProductUrl = "/product/add";

		public const string AddSoldProductUrl = "/product/sold/add";
		public const string AddSoldProductListUrl = "/product/sold/multiple";
		public const string SoldProductListUrl = "/product/sold/all";
		public const string SoldProductByParamUrl = "/product/sold/{param}";

		public const string SoldLogsByParamUrl = "/sold/logs/{param}";

		public const string SigInUserUrl = "/user/sigin";
		public const string SignUpUserUrl = "/user/signup";

		public const string UserProductReportUrl = "/user/product/report/all";
		public const string UserPurchaseReportByParamUrl = "/user/purchase/report/{param}";

		public const string UserByParamUrl = "/user/{param}";
		public const string UserListUrl = "/user/all";
		public const string AddUserUrl = SignUpUserUrl;

		public const string VatByParamUrl = "/vat/{param}";
		public const string VatListUrl = "/vat/all";
		public const string CalculateVatUrl = "/vat/calculate";

		public const string CompanyByParamUrl = "/company/{param}";
		public const string CompanyListUrl = "/company/all";
		public const string AddCompanyUrl = "/company/add";

		public const string CalculateCompanyVatByParamUrl = "/company/vat/{param}";
		public const string CompanyVatListUrl = "/company/vat/all";
		public const string CalculateCompanyVatUrl = "/company/vat/calculate";
	}
}
