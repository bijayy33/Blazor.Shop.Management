﻿using System;

namespace Blazor.Shop.Management.Common.Enums
{
	public enum ActionType
	{
		LoadMore = 0,
		Fetch = 1,
		FetchAll = 2,
		Add = 3,
		Edit = 4,
		Delete = 5,
		Restore = 6,
		None = 7
	}
}
