﻿namespace Blazor.Shop.Management.Common.Enums
{
	public enum TaxType
	{
		PAN = 1,
		VAT = 2
	}
}
