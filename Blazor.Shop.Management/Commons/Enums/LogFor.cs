﻿namespace Blazor.Shop.Management.Common.Enums
{
	public enum LogFor
	{
		User = 1,
		Product = 2,
	}
}
