﻿namespace Blazor.Shop.Management.Common.Enums
{
	public enum QuantityType
	{
		PCS = 1,
		SET = 2
	}
}
