﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blazor.Shop.Management.Common.Enums
{
	public enum PriceFor
	{
		Mrp = 1,
		CostPrice = 2,
		SellingPrice = 3
	}
}
