﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.Management.Common.Constants;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

namespace Blazor.Shop.Management.Services
{
	public class ShopManagementService
	{
		#region Fields and Variables

		private readonly HttpClient _httpClient;
		private readonly ILogger<ShopManagementService> _logger;

		private const string apiUrl = "api/shop/";
		private const string productUrl = "product/";
		private const string soldProductUrl = "sold/";
		private const string symbolicPriceUrl = "symbolic-price/";
		private const string vatUrl = "vat/";
		private const string allUrl = "all/";
		private const string multipleUrl = "multiple/";

		#endregion Fields and Variables

		#region Constructors

		public ShopManagementService(
			HttpClient httpClient,
			ILogger<ShopManagementService> logger)
		{
			this._httpClient = httpClient;
			this._logger = logger;
		}

		#endregion Constructors

		#region Product

		public async Task<List<ProductViewModel>> GetProductsAsync(CancellationToken cancellationToken = default)
		{
			try
			{
				List<ProductViewModel> products = new List<ProductViewModel>();

				string request = $"{ apiUrl }{ productUrl }{ allUrl }";
				HttpResponseMessage response = await this._httpClient.GetAsync(request, cancellationToken);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					var result = response.Content.ReadAsStringAsync().Result;
					products = JsonConvert.DeserializeObject<List<ProductViewModel>>(result);
				}

				return products;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<ProductViewModel> AddProductAsync(ProductViewModel viewModel, CancellationToken cancellationToken = default)
		{
			try
			{
				string requestUrl = $"{ apiUrl }{ productUrl }";
				string content = JsonConvert.SerializeObject(viewModel);
				var buffer = System.Text.Encoding.UTF8.GetBytes(content);
				var byteContent = new ByteArrayContent(buffer);
				byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				HttpResponseMessage response = await this._httpClient.PostAsync(requestUrl, byteContent, cancellationToken);
				var result = response.Content.ReadAsStringAsync().Result;
				ProductViewModel product = JsonConvert.DeserializeObject<ProductViewModel>(result);
				return product;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<IEnumerable<ProductViewModel>> AddProductsAsync(IEnumerable<ProductViewModel> viewModels, CancellationToken cancellationToken = default)
		{
			try
			{
				string requestUrl = $"{ apiUrl }{ symbolicPriceUrl}{multipleUrl}";
				string content = JsonConvert.SerializeObject(viewModels);
				var buffer = System.Text.Encoding.UTF8.GetBytes(content);
				var byteContent = new ByteArrayContent(buffer);
				byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				HttpResponseMessage response = await this._httpClient.PostAsync(requestUrl, byteContent, cancellationToken);
				var result = response.Content.ReadAsStringAsync().Result;
				IEnumerable<ProductViewModel> products = JsonConvert.DeserializeObject<IEnumerable<ProductViewModel>>(result);
				return products;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<ProductViewModel> UpdateProductAsync(ProductViewModel viewModel, CancellationToken cancellationToken = default)
		{
			try
			{
				string requestUrl = $"{ apiUrl }{ productUrl}{ viewModel.Id }";
				string content = JsonConvert.SerializeObject(viewModel);
				var buffer = System.Text.Encoding.UTF8.GetBytes(content);
				var byteContent = new ByteArrayContent(buffer);
				byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				HttpResponseMessage response = await this._httpClient.PutAsync(requestUrl, byteContent, cancellationToken);
				var result = response.Content.ReadAsStringAsync().Result;
				ProductViewModel product = JsonConvert.DeserializeObject<ProductViewModel>(result);
				return product;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<ProductViewModel> DeleteProductAsync(string productId, CancellationToken cancellationToken = default)
		{
			try
			{
				string requestUrl = $"{ apiUrl }{ productUrl }{ productId }";
				HttpResponseMessage response = await this._httpClient.DeleteAsync(requestUrl, cancellationToken);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					var result = response.Content.ReadAsStringAsync().Result;
					ProductViewModel product = JsonConvert.DeserializeObject<ProductViewModel>(result);
					return product;
				}
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		#endregion Product

		#region Sold Product

		public async Task<List<SoldViewModel>> GetSoldsAsync(CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{WebApiUrl.AllSoldListUrl}";
				HttpResponseMessage response = await this._httpClient.GetAsync(request, cancellationToken);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					var result = response.Content.ReadAsStringAsync().Result;
					List<SoldViewModel> soldProducts = JsonConvert.DeserializeObject<List<SoldViewModel>>(result);
					return soldProducts;
				}

				return new List<SoldViewModel>();
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<SoldViewModel> GetSoldByIdAsync(string soldId, CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{ WebApiUrl.SoldByParamUrl}".Replace($"{WebApiUrl.Param }", soldId);
				HttpResponseMessage response = await this._httpClient.GetAsync(request, cancellationToken);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					var result = response.Content.ReadAsStringAsync().Result;
					SoldViewModel soldProducts = JsonConvert.DeserializeObject<SoldViewModel>(result);
					return soldProducts;
				}
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<IEnumerable<SoldViewModel>> GetSoldsByUserIdAsync(string userId, CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{ WebApiUrl.SoldByParamUrl}".Replace($"{WebApiUrl.Param }", $"user/{userId}");
				HttpResponseMessage response = await this._httpClient.GetAsync(request, cancellationToken);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					var result = response.Content.ReadAsStringAsync().Result;
					IEnumerable<SoldViewModel> soldProducts = JsonConvert.DeserializeObject<IEnumerable<SoldViewModel>>(result);
					return soldProducts;
				}
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<SoldViewModel> AddSoldProductAsync(SoldViewModel viewModel, CancellationToken cancellationToken = default)
		{
			try
			{
				string requestUrl = $"{ WebApiUrl.AddSoldUrl }";
				string content = JsonConvert.SerializeObject(viewModel);
				var buffer = System.Text.Encoding.UTF8.GetBytes(content);
				var byteContent = new ByteArrayContent(buffer);
				byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				HttpResponseMessage response = await this._httpClient.PostAsync(requestUrl, byteContent, cancellationToken);

				if (response.StatusCode == HttpStatusCode.Created)
				{
					var result = response.Content.ReadAsStringAsync().Result;
					SoldViewModel soldProduct = JsonConvert.DeserializeObject<SoldViewModel>(result);
					return soldProduct;
				}
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<SoldViewModel> UpdateSoldProductAsync(SoldViewModel viewModel, CancellationToken cancellationToken = default)
		{
			try
			{
				string requestUrl = $"{ WebApiUrl.SoldByParamUrl}".Replace($"{WebApiUrl.Param }", viewModel.Id);
				string content = JsonConvert.SerializeObject(viewModel);
				var buffer = System.Text.Encoding.UTF8.GetBytes(content);
				var byteContent = new ByteArrayContent(buffer);
				byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				HttpResponseMessage response = await this._httpClient.PutAsync(requestUrl, byteContent, cancellationToken);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					var result = response.Content.ReadAsStringAsync().Result;
					SoldViewModel soldProduct = JsonConvert.DeserializeObject<SoldViewModel>(result);
					return soldProduct;
				}
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<SoldViewModel> DeleteSoldById(string soldId, CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{WebApiUrl.SoldByParamUrl}".Replace(WebApiUrl.Param, soldId);
				var respone = await this._httpClient.SendJsonAsync<SoldViewModel>(HttpMethod.Delete, request, null);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<SoldProductViewModel> DeleteSoldProductById(string soldProductId, CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{WebApiUrl.SoldProductByParamUrl}".Replace(WebApiUrl.Param, soldProductId);
				var respone = await this._httpClient.SendJsonAsync<SoldProductViewModel>(HttpMethod.Delete, request, null);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		#endregion Sold Product

		#region Product VAT

		public async Task<List<VatViewModel>> GetVatsAsync(CancellationToken cancellationToken = default)
		{
			List<VatViewModel> responseViewModel = new List<VatViewModel>();

			try
			{
				string request = $"{ apiUrl }{productUrl}{vatUrl}{ allUrl }";
				HttpResponseMessage response = await this._httpClient.GetAsync(request, cancellationToken);

				var result = response.Content.ReadAsStringAsync().Result;
				responseViewModel = JsonConvert.DeserializeObject<List<VatViewModel>>(result);
				return responseViewModel;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return responseViewModel;
		}

		public async Task<VatViewModel> GetVatByInvoiceNumberAsync(string invoiceNumber, CancellationToken cancellationToken = default)
		{
			VatViewModel responseViewModel = new VatViewModel();

			try
			{
				string request = $"{WebApiUrl.VatByParamUrl}{invoiceNumber}";
				var index = request.IndexOf(WebApiUrl.Param);
				request = request.Remove(index, WebApiUrl.Param.Count());
				HttpResponseMessage response = await this._httpClient.GetAsync(request, cancellationToken);

				var result = response.Content.ReadAsStringAsync().Result;
				responseViewModel = JsonConvert.DeserializeObject<VatViewModel>(result);
				return responseViewModel;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return responseViewModel;
		}

		public async Task<VatViewModel> AddVatAsync(VatViewModel viewModel, CancellationToken cancellationToken = default)
		{
			VatViewModel responseViewModel = new VatViewModel();

			try
			{
				string requestUrl = $"{ apiUrl }{ productUrl}{vatUrl}";
				string content = JsonConvert.SerializeObject(viewModel);
				var buffer = System.Text.Encoding.UTF8.GetBytes(content);
				var byteContent = new ByteArrayContent(buffer);
				byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				HttpResponseMessage response = await this._httpClient.PostAsync(requestUrl, byteContent, cancellationToken);

				if (response.StatusCode == HttpStatusCode.Created)
				{
					string result = response.Content.ReadAsStringAsync().Result;
					responseViewModel = JsonConvert.DeserializeObject<VatViewModel>(result);

					return responseViewModel;
				}
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		#endregion Sold VAT

		#region User Product Report

		public async Task<UserProductReportViewModel> GetUserProductsReportAsync(string phoneNumber, CancellationToken cancellationToken = default)
		{
			UserProductReportViewModel responseViewModel = new UserProductReportViewModel();

			try
			{
				string request = $"{ apiUrl }user/{productUrl}report";
				HttpResponseMessage response = await this._httpClient.GetAsync(request, cancellationToken);

				var result = response.Content.ReadAsStringAsync().Result;
				responseViewModel = JsonConvert.DeserializeObject<UserProductReportViewModel>(result);
				return responseViewModel;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return responseViewModel;
		}

		public async Task<List<UserPurchaseViewModel>> GetUserPurchaseReportAsync(CancellationToken cancellationToken = default)
		{
			List<UserPurchaseViewModel> responseViewModel = new List<UserPurchaseViewModel>();

			try
			{
				string request = $"{ apiUrl }user/{productUrl}purchase";
				HttpResponseMessage response = await this._httpClient.GetAsync(request, cancellationToken);

				var result = response.Content.ReadAsStringAsync().Result;
				responseViewModel = JsonConvert.DeserializeObject<List<UserPurchaseViewModel>>(result);
				return responseViewModel;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return responseViewModel;
		}

		#endregion User Product Report

		#region Symbolic Prices

		public async Task<decimal> GetPriceBySymbolicPriceAsync(string symbolicPrice, CancellationToken cancellationToken = default)
		{
			decimal actualPrice = -1;
			try
			{
				string requestUrl = $"{ apiUrl }price/{symbolicPrice}";
				HttpResponseMessage response = await this._httpClient.GetAsync(requestUrl, cancellationToken);
				string result = response.Content.ReadAsStringAsync().Result;
				decimal.TryParse(result, out actualPrice);
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return actualPrice;
		}

		public async Task<List<SymbolicPriceViewModel>> GetSymbolicPricesAsync(CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{ apiUrl }{ symbolicPriceUrl }{ allUrl }";
				HttpResponseMessage response = await this._httpClient.GetAsync(request, cancellationToken);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					var result = response.Content.ReadAsStringAsync().Result;
					List<SymbolicPriceViewModel> symbolicPrices = JsonConvert.DeserializeObject<List<SymbolicPriceViewModel>>(result);
					return symbolicPrices;
				}

				if (response.StatusCode == HttpStatusCode.NotFound)
					return new List<SymbolicPriceViewModel>();
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<SymbolicPriceViewModel> AddSymbolicPricesAsync(SymbolicPriceViewModel viewModel, CancellationToken cancellationToken = default)
		{
			try
			{
				string requestUrl = $"{ apiUrl }{ symbolicPriceUrl }";
				string content = JsonConvert.SerializeObject(viewModel);
				var buffer = System.Text.Encoding.UTF8.GetBytes(content);
				var byteContent = new ByteArrayContent(buffer);
				byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				HttpResponseMessage response = await this._httpClient.PostAsync(requestUrl, byteContent, cancellationToken);

				if (response.StatusCode == HttpStatusCode.Created)
				{
					var result = response.Content.ReadAsStringAsync().Result;
					SymbolicPriceViewModel symbolicPrice = JsonConvert.DeserializeObject<SymbolicPriceViewModel>(result);
					return symbolicPrice;
				}
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<SymbolicPriceViewModel> UpdateSymbolicPriceAsync(SymbolicPriceViewModel viewModel, CancellationToken cancellationToken = default)
		{
			try
			{
				string requestUrl = $"{ apiUrl }{ symbolicPriceUrl}{ viewModel.Id }";
				string content = JsonConvert.SerializeObject(viewModel);
				var buffer = System.Text.Encoding.UTF8.GetBytes(content);
				var byteContent = new ByteArrayContent(buffer);
				byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				HttpResponseMessage response = await this._httpClient.PutAsync(requestUrl, byteContent, cancellationToken);
				var result = response.Content.ReadAsStringAsync().Result;
				SymbolicPriceViewModel soldProduct = JsonConvert.DeserializeObject<SymbolicPriceViewModel>(result);
				return soldProduct;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<SymbolicPriceViewModel> DeleteSymbolicPriceAsync(string id, CancellationToken cancellationToken = default)
		{
			try
			{
				string requestUrl = $"{ apiUrl }{ symbolicPriceUrl }{ id }";
				HttpResponseMessage response = await this._httpClient.DeleteAsync(requestUrl, cancellationToken);
				var result = response.Content.ReadAsStringAsync().Result;
				SymbolicPriceViewModel soldProduct = JsonConvert.DeserializeObject<SymbolicPriceViewModel>(result);
				return soldProduct;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public decimal GetPrice(string symbolicPrice, IEnumerable<SymbolicPriceViewModel> symbolicPrices = null)
		{
			decimal value = 0;
			string result = string.Empty;
			bool notNumber = false;

			if (!string.IsNullOrWhiteSpace(symbolicPrice) && !decimal.TryParse(symbolicPrice, out value))
			{
				if (symbolicPrices == null || !symbolicPrices.Any())
				{
					symbolicPrices = this.GetSymbolicPricesAsync().Result;
				}

				foreach (char c in symbolicPrice)
				{
					var price = symbolicPrices.FirstOrDefault(p => p.Alphabet == c);
					result += price == null ? c + string.Empty : price.Value + string.Empty;
				}

				notNumber = true;
			}

			if (notNumber)
			{
				decimal.TryParse(result, out value);
			}

			return value;
		}

		public string GetEncodedPrice(decimal actualPrice, IEnumerable<SymbolicPriceViewModel> symbolicPrices = null)
		{
			string result = string.Empty;

			if (actualPrice >= 0)
			{
				if (symbolicPrices == null || !symbolicPrices.Any())
				{
					symbolicPrices = this.GetSymbolicPricesAsync().Result;
				}

				foreach (char c in actualPrice.ToString())
				{
					var price = symbolicPrices.FirstOrDefault(p => p.Value.ToString() == c + string.Empty);
					result += price == null ? c + string.Empty : price.Alphabet + string.Empty;
				}
			}

			return result;
		}

		#endregion Symbolic Prices

		#region Company

		public async Task<CompanyViewModel> GetCompanyByTaxTypeNumber(string taxTypeNumber, CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{WebApiUrl.CompanyByParamUrl}".Replace(WebApiUrl.Param, taxTypeNumber);
				var respone = await this._httpClient.GetJsonAsync<CompanyViewModel>(request);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<CompanyViewModel> GetCompanyById(string companyId, CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{WebApiUrl.CompanyByParamUrl}".Replace(WebApiUrl.Param, companyId);
				var respone = await this._httpClient.GetJsonAsync<CompanyViewModel>(request);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<List<CompanyViewModel>> GetCompanies(CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{WebApiUrl.CompanyListUrl}";
				var respone = await this._httpClient.GetJsonAsync<List<CompanyViewModel>>(request);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<CompanyViewModel> AddCompany(CompanyViewModel company, CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{WebApiUrl.AddCompanyUrl}";
				var respone = await this._httpClient.PostJsonAsync<CompanyViewModel>(request, company);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<CompanyViewModel> UpdateCompany(string companyId, CompanyViewModel company, CancellationToken cancellationToken = default)
		{
			try
			{
				if (string.IsNullOrWhiteSpace(companyId) || companyId != company.Id)
				{
					return company;
				}

				string request = $"{WebApiUrl.CompanyByParamUrl}".Replace(WebApiUrl.Param, companyId);
				var respone = await this._httpClient.PutJsonAsync<CompanyViewModel>(request, company);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<CompanyViewModel> DeleteCompanyById(string companyId, CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{WebApiUrl.CompanyByParamUrl}".Replace(WebApiUrl.Param, companyId);
				var respone = await this._httpClient.SendJsonAsync<CompanyViewModel>(HttpMethod.Delete, request, null);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}


		#endregion Company

		#region Company VAT

		public async Task<CompanyVatViewModel> GetCompanyVatByBillNumber(string billNumber, CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{WebApiUrl.CompanyVatByParamUrl}".Replace(WebApiUrl.Param, billNumber);
				var respone = await this._httpClient.GetJsonAsync<CompanyVatViewModel>(request);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<CompanyVatViewModel> GetCompanyVatById(string companyVatId, CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{WebApiUrl.CompanyVatByParamUrl}".Replace(WebApiUrl.Param, companyVatId);
				var respone = await this._httpClient.GetJsonAsync<CompanyVatViewModel>(request);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<List<CompanyVatViewModel>> GetCompanyVats(CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{WebApiUrl.CompanyVatListUrl}";
				var respone = await this._httpClient.GetJsonAsync<List<CompanyVatViewModel>>(request);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<CompanyVatViewModel> AddCompanyVat(CompanyVatViewModel companyVat, CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{WebApiUrl.AddCompanyVatUrl}";
				var respone = await this._httpClient.PostJsonAsync<CompanyVatViewModel>(request, companyVat);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<CompanyVatViewModel> UpdateCompanyVat(string companyVatId, CompanyVatViewModel companyVat, CancellationToken cancellationToken = default)
		{
			try
			{
				if (string.IsNullOrWhiteSpace(companyVatId) || companyVatId != companyVat.Id)
				{
					return companyVat;
				}

				string request = $"{WebApiUrl.CompanyVatByParamUrl}".Replace(WebApiUrl.Param, companyVatId);
				var respone = await this._httpClient.PutJsonAsync<CompanyVatViewModel>(request, companyVat);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<CompanyVatViewModel> DeleteCompanyVatById(string companyVatId, CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{WebApiUrl.CompanyVatByParamUrl}".Replace(WebApiUrl.Param, companyVatId);
				var respone = await this._httpClient.SendJsonAsync<CompanyVatViewModel>(HttpMethod.Delete, request, null);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		#endregion Company VAT

		#region Sold Logs

		public async Task<List<SoldLogViewModel>> GetLogsBySoldId(string soldId, CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{WebApiUrl.SoldLogsByParamUrl}".Replace(WebApiUrl.Param, soldId);
				var respone = await this._httpClient.GetJsonAsync<List<SoldLogViewModel>>(request);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		public async Task<SoldLogViewModel> DeleteSoldLogById(string logId, CancellationToken cancellationToken = default)
		{
			try
			{
				string request = $"{WebApiUrl.SoldLogsByParamUrl}".Replace(WebApiUrl.Param, logId);
				var respone = await this._httpClient.SendJsonAsync<SoldLogViewModel>(HttpMethod.Delete, request, null);
				return respone;
			}
			catch (Exception ex)
			{
				this._logger.LogError(ex.Message, ex);
			}

			return null;
		}

		#endregion Sold Logs
	}
}
