﻿using System;

namespace Blazor.Shop.Management.Services
{
	public class AppStateService
	{
		public bool IsCancelClicked { get; set; }
		public bool IsSearchMore { get; set; }
		public bool IsLoadMore { get; set; } = true;

		public event Action OnChange;

		public void NotifyStateChanged() => this.OnChange?.Invoke();
	}
}
