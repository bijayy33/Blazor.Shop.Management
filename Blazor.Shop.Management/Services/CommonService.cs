﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Blazor.Shop.Management.Common.Constants;

namespace Blazor.Shop.Management.Services
{
	public class CommonService
	{
		public async Task<List<string>> GetRoles()
		{
			return await Task.FromResult(new List<string>
			{
				DefaultRole.Seller,
				DefaultRole.Buyer
			});
		}
	}
}
