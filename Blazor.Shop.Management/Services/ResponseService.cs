﻿using System.Collections.Generic;

using Blazor.Shop.Management.Common.Enums;
using Blazor.Shop.Management.ViewModels;

namespace Blazor.Shop.Management.Services
{
	public class ResponseService
	{
		public List<T> Create<T>(List<T> viewModels = null,
			T viewModel = null,
			ActionType type = ActionType.None) where T : BaseViewModel, new()
		{
			if (viewModels != null)
			{
				switch (type)
				{
					case ActionType.Add:
						viewModels.Insert(0, viewModel);
						break;

					case ActionType.Edit:
						int index = viewModels.FindIndex(x => x.Id == viewModel.Id);
						viewModels[index] = viewModel;
						break;

					case ActionType.Delete:
						viewModels.RemoveAll(x => x.Id == viewModel.Id);
						break;

					default:
						break;
				}
			}

			return viewModels;
		}
		
		public T Create<T>(ref List<T> viewModels,
			T viewModel = null,
			ActionType type = ActionType.None) where T : BaseViewModel, new()
		{
			if (viewModels != null)
			{
				switch (type)
				{
					case ActionType.Add:
						viewModels.Insert(0, viewModel);
						break;

					case ActionType.Edit:
						int index = viewModels.FindIndex(x => x.Id == viewModel.Id);
						viewModels[index] = viewModel;
						break;

					case ActionType.Delete:
						viewModels.RemoveAll(x => x.Id == viewModel.Id);
						break;

					default:
						break;
				}
			}

			return viewModel;
		}
	}
}
