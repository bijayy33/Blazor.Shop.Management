﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.Management.ViewModels;

using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

namespace Blazor.Shop.Management.Services
{
	public class UserManagementService
	{
		private readonly HttpClient httpClient;
		private readonly ILogger<UserManagementService> logger;
		private const string apiUrl = "api/user/";
		private const string logInUrl = "login/";
		private const string logOutUrl = "logout/";
		private const string addUrl = "add/";
		private const string updateUrl = "update/";
		private const string allUrl = "all/";

		public UserManagementService(
			HttpClient httpClient,
			ILogger<UserManagementService> logger)
		{
			this.httpClient = httpClient;
			this.logger = logger;
		}

		public async Task<ResponseViewModel<UserViewModel>> LogInAsync(SignInViewModel viewModel, CancellationToken cancellationToken = default)
		{
			ResponseViewModel<UserViewModel> responseViewModel = new ResponseViewModel<UserViewModel>();

			try
			{
				string requestUrl = $"{ apiUrl }{ logInUrl}";
				string content = JsonConvert.SerializeObject(viewModel);
				var buffer = System.Text.Encoding.UTF8.GetBytes(content);
				var byteContent = new ByteArrayContent(buffer);
				byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				HttpResponseMessage response = await this.httpClient.PostAsync(requestUrl, byteContent, cancellationToken);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					string result = response.Content.ReadAsStringAsync().Result;
					responseViewModel = JsonConvert.DeserializeObject<ResponseViewModel<UserViewModel>>(result);
				}
				else
				{
					responseViewModel.StatusCode = response.StatusCode;
				}

				return responseViewModel;
			}
			catch (Exception ex)
			{
				this.logger.LogError(ex.Message, ex);
				responseViewModel.StatusCode = HttpStatusCode.InternalServerError;
			}

			return responseViewModel;
		}

		public async Task<ResponseViewModel<object>> LogOutAsync(CancellationToken cancellationToken = default)
		{
			ResponseViewModel<object> responseViewModel = new ResponseViewModel<object>();

			try
			{
				string requestUrl = $"{ apiUrl }{ logOutUrl}";
				string content = JsonConvert.SerializeObject(null);
				var buffer = System.Text.Encoding.UTF8.GetBytes(content);
				var byteContent = new ByteArrayContent(buffer);
				byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				HttpResponseMessage response = await this.httpClient.PostAsync(requestUrl, byteContent, cancellationToken);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					var result = response.Content.ReadAsStringAsync().Result;
					responseViewModel = JsonConvert.DeserializeObject<ResponseViewModel<object>>(result);
				}
				else
				{
					responseViewModel.StatusCode = response.StatusCode;
				}

				return responseViewModel;
			}
			catch (Exception ex)
			{
				this.logger.LogError(ex.Message, ex);
				responseViewModel.StatusCode = HttpStatusCode.InternalServerError;
			}

			return responseViewModel;
		}

		public async Task<ResponseViewModel<List<UserViewModel>>> GetUsersAsync(CancellationToken cancellationToken = default)
		{
			ResponseViewModel<List<UserViewModel>> responseViewModel = new ResponseViewModel<List<UserViewModel>>();

			try
			{
				string request = $"{ apiUrl }{ allUrl }";
				HttpResponseMessage response = await this.httpClient.GetAsync(request, cancellationToken);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					var result = response.Content.ReadAsStringAsync().Result;
					responseViewModel = JsonConvert.DeserializeObject<ResponseViewModel<List<UserViewModel>>>(result);
				}
				else
				{
					responseViewModel.StatusCode = response.StatusCode;
				}
			}
			catch (Exception ex)
			{
				this.logger.LogError(ex.Message, ex);
				responseViewModel.StatusCode = HttpStatusCode.InternalServerError;
			}

			return responseViewModel;
		}
		
		public async Task<ResponseViewModel<List<UserViewModel>>> GetUsersByPageAsync(int skip = 0, int take = 50, CancellationToken cancellationToken = default)
		{
			ResponseViewModel<List<UserViewModel>> responseViewModel = new ResponseViewModel<List<UserViewModel>>();

			try
			{
				string request = $"{ apiUrl }{ allUrl }";
				HttpResponseMessage response = await this.httpClient.GetAsync(request, cancellationToken);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					var result = response.Content.ReadAsStringAsync().Result;
					responseViewModel = JsonConvert.DeserializeObject<ResponseViewModel<List<UserViewModel>>>(result);
				}
				else
				{
					responseViewModel.StatusCode = response.StatusCode;
				}
			}
			catch (Exception ex)
			{
				this.logger.LogError(ex.Message, ex);
				responseViewModel.StatusCode = HttpStatusCode.InternalServerError;
			}

			return responseViewModel;
		}

		public async Task<ResponseViewModel<List<UserViewModel>>> SearchUsersByColumNameAsync(string columName, string searchText, CancellationToken cancellationToken = default)
		{
			ResponseViewModel<List<UserViewModel>> responseViewModel = new ResponseViewModel<List<UserViewModel>>();

			try
			{
				string request = $"{ apiUrl }{ allUrl }";
				HttpResponseMessage response = await this.httpClient.GetAsync(request, cancellationToken);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					var result = response.Content.ReadAsStringAsync().Result;
					responseViewModel = JsonConvert.DeserializeObject<ResponseViewModel<List<UserViewModel>>>(result);
				}
				else
				{
					responseViewModel.StatusCode = response.StatusCode;
				}
			}
			catch (Exception ex)
			{
				this.logger.LogError(ex.Message, ex);
				responseViewModel.StatusCode = HttpStatusCode.InternalServerError;
			}

			return responseViewModel;
		}
		
		public async Task<ResponseViewModel<List<UserViewModel>>> SearchUsersByExpressionAsync(Expression<Func<UserViewModel, bool>> expression, CancellationToken cancellationToken = default)
		{
			ResponseViewModel<List<UserViewModel>> responseViewModel = new ResponseViewModel<List<UserViewModel>>();

			try
			{
				string request = $"{ apiUrl }{ allUrl }";
				HttpResponseMessage response = await this.httpClient.GetAsync(request, cancellationToken);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					var result = response.Content.ReadAsStringAsync().Result;
					responseViewModel = JsonConvert.DeserializeObject<ResponseViewModel<List<UserViewModel>>>(result);
				}
				else
				{
					responseViewModel.StatusCode = response.StatusCode;
				}
			}
			catch (Exception ex)
			{
				this.logger.LogError(ex.Message, ex);
				responseViewModel.StatusCode = HttpStatusCode.InternalServerError;
			}

			return responseViewModel;
		}

		public async Task<ResponseViewModel<UserViewModel>> SignUpUserAsync(UserViewModel viewModel, CancellationToken cancellationToken = default)
		{
			ResponseViewModel<UserViewModel> responseViewModel = new ResponseViewModel<UserViewModel>();

			try
			{
				string requestUrl = $"{ apiUrl }{ addUrl}";
				string content = JsonConvert.SerializeObject(viewModel);
				var buffer = System.Text.Encoding.UTF8.GetBytes(content);
				var byteContent = new ByteArrayContent(buffer);
				byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				HttpResponseMessage response = await this.httpClient.PostAsync(requestUrl, byteContent, cancellationToken);

				if (response.StatusCode == HttpStatusCode.Created)
				{
					string result = response.Content.ReadAsStringAsync().Result;
					responseViewModel = JsonConvert.DeserializeObject<ResponseViewModel<UserViewModel>>(result);
				}
				else
				{
					responseViewModel.StatusCode = response.StatusCode;
				}

				return responseViewModel;
			}
			catch (Exception ex)
			{
				this.logger.LogError(ex.Message, ex);
				responseViewModel.StatusCode = HttpStatusCode.InternalServerError;
			}

			return responseViewModel;
		}

		public async Task<ResponseViewModel<UserViewModel>> UpdateUserAsync(UserViewModel viewModel, CancellationToken cancellationToken = default)
		{
			ResponseViewModel<UserViewModel> responseViewModel = new ResponseViewModel<UserViewModel>();

			try
			{
				string requestUrl = $"{ apiUrl }{ updateUrl}{ viewModel.Id }";
				string content = JsonConvert.SerializeObject(viewModel);
				var buffer = System.Text.Encoding.UTF8.GetBytes(content);
				var byteContent = new ByteArrayContent(buffer);
				byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				HttpResponseMessage response = await this.httpClient.PutAsync(requestUrl, byteContent, cancellationToken);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					string result = response.Content.ReadAsStringAsync().Result;
					responseViewModel = JsonConvert.DeserializeObject<ResponseViewModel<UserViewModel>>(result);
				}
				else
				{
					responseViewModel.StatusCode = response.StatusCode;
				}

				return responseViewModel;
			}
			catch (Exception ex)
			{
				this.logger.LogError(ex.Message, ex);
				responseViewModel.StatusCode = HttpStatusCode.InternalServerError;
			}

			return responseViewModel;
		}

		public async Task<ResponseViewModel<UserViewModel>> DeleteUserAsync(string userId, CancellationToken cancellationToken = default)
		{
			ResponseViewModel<UserViewModel> responseViewModel = new ResponseViewModel<UserViewModel>();

			try
			{
				string requestUrl = $"{ apiUrl }{ userId }";
				HttpResponseMessage response = await this.httpClient.DeleteAsync(requestUrl, cancellationToken);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					string result = response.Content.ReadAsStringAsync().Result;
					responseViewModel = JsonConvert.DeserializeObject<ResponseViewModel<UserViewModel>>(result);
				}
				else
				{
					responseViewModel.StatusCode = response.StatusCode;
				}

				return responseViewModel;
			}
			catch (Exception ex)
			{
				this.logger.LogError(ex.Message, ex);
				responseViewModel.StatusCode = HttpStatusCode.InternalServerError;
			}

			return responseViewModel;
		}
	}
}
