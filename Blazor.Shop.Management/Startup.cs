using System;
using System.Net.Http;

using Blazor.Shop.Management.Common.Constants;
using Blazor.Shop.Management.Services;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.JSInterop;

namespace Blazor.Shop.Management
{
	public class Startup
	{
		private readonly IConfiguration configuration;
		private readonly IHostEnvironment environment;

		public Startup(IConfiguration configuration, IHostEnvironment environment)
		{
			this.configuration = configuration;
			this.environment = environment;
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddRazorPages();
			services.AddServerSideBlazor();
			services.AddScoped<ResponseService, ResponseService>();
			services.AddScoped<AppStateService, AppStateService>();

			Uri webApiBaseAddress = new Uri(configuration.GetSection("WebApiUrl").GetSection("Url").Value);

			services.AddHttpClient<ShopManagementService, ShopManagementService>(client =>
			{
				client.BaseAddress = webApiBaseAddress;
			})
			//Code from this place https://stackoverflow.com/questions/38138952/bypass-invalid-ssl-certificate-in-net-core
			.ConfigurePrimaryHttpMessageHandler(() =>
			{
				var handler = new HttpClientHandler();

				if (this.environment.IsDevelopment())
				{
					handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
				}
				return handler;
			});

			services.AddHttpClient<UserManagementService, UserManagementService>(client =>
			{
				client.BaseAddress = webApiBaseAddress;
			})
			//Code from this place https://stackoverflow.com/questions/38138952/bypass-invalid-ssl-certificate-in-net-core
			.ConfigurePrimaryHttpMessageHandler(() =>
			{
				var handler = new HttpClientHandler();

				if (this.environment.IsDevelopment())
				{
					handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
				}
				return handler;
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseStaticFiles();
			app.UseAuthentication();
			app.UseRouting();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapBlazorHub();
				endpoints.MapFallbackToPage("/_Host");
			});
		}

		public static void AddApplicationAuthentication(IServiceCollection services, IConfiguration config)
		{
			services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
			 .AddJwtBearer(jwtBearerOptions =>
			 {
				 jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters()
				 {
					 ValidateIssuer = true,
					 ValidateAudience = true,
					 ValidateLifetime = false,
					 ValidateIssuerSigningKey = true,
					 ValidIssuer = config.GetSection("JWT:Issuer").Value,
					 ValidAudience = config.GetSection("JWT:Audience").Value,
					 IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(config.GetSection("JWT:SigningKey").Value))
				 };
			 });
		}

		public static void AddClaimBasedAuthorization(IServiceCollection services)
		{
			services.AddAuthorization(options =>
			{
				options.AddPolicy(DefaultRole.SuperAdmin, policy => policy.RequireRole(DefaultRole.SuperAdmin));
				options.AddPolicy(DefaultRole.Seller, policy => policy.RequireRole(DefaultRole.Seller));
				options.AddPolicy(DefaultRole.Buyer, policy => policy.RequireRole(DefaultRole.Buyer));
			});
		}
	}
}
