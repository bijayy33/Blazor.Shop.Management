﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.Management.Common.Constants;
using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.JSInterop;

namespace Blazor.Shop.Management.Pages.Products.Vats
{
	public class CalculateVatComponentBase : ComponentBase, IDisposable
	{
		private CancellationTokenSource CancellationTokenSource = new CancellationTokenSource();

		[Parameter]
		public string Redirect { get; set; }

		[Parameter]
		public List<VatViewModel> ViewModels { get; set; }

		[Inject]
		protected ShopManagementService shopService { get; set; }

		[Inject]
		protected IJSRuntime jSRuntime { get; set; }

		[Inject]
		protected NavigationManager navigationManager { get; set; }

		[Parameter]
		public EventCallback<VatViewModel> OnAddSuccess { get; set; }

		[Parameter]
		public EventCallback OnCancelSuccess { get; set; }
		protected EditContext editContext { get; set; }
		protected ValidationMessageStore messageStore { get; set; }
		protected IEnumerable<string> productDescriptions { get; set; } = new List<string>();
		protected IEnumerable<string> productCodes { get; set; } = new List<string>();
		protected IEnumerable<ProductViewModel> productViewModels { get; set; }
		protected VatViewModel AddOrEditViewModel { get; set; }
		protected ProductVatViewModel productVatViewModel { get; set; }
		protected List<ProductVatViewModel> viewModels { get; set; }
		protected bool isInProgress { get; set; }
		protected bool isAlreadyAdded { get; set; }
		protected bool invalidViewModel { get; set; }
		protected bool isSuccess { get; set; }
		protected bool submitClicked { get; set; }

		protected async override Task OnInitializedAsync()
		{
			this.isInProgress = true;
			this.productVatViewModel = new ProductVatViewModel();
			this.viewModels = new List<ProductVatViewModel>();
			this.editContext = new EditContext(this.productVatViewModel);
			this.messageStore = new ValidationMessageStore(this.editContext);

			if (string.IsNullOrEmpty(this.Redirect))
			{
				this.productViewModels = await this.shopService.GetProductsAsync();
				if (this.productViewModels != null)
				{
					this.productDescriptions = this.productViewModels.Select(x => x.Description);
					this.productCodes = this.productViewModels.Select(x => x.Code);
				}
			}
			else
			{
				var vatViewModel = await this.shopService.GetVatByInvoiceNumberAsync(this.Redirect.Trim());
			}
			this.isInProgress = false;
		}

		protected async Task OnSubmitAsync()
		{
			if (this.editContext.Validate())
			{
			}
		}

		protected async Task OnPrintAsync()
		{
			this.isInProgress = true;
			var response = await this.shopService.AddVatAsync(AddOrEditViewModel, CancellationTokenSource.Token);

			if (response == null)
			{
				this.submitClicked = true;
				this.isSuccess = false;
			}
			else
			{
				this.isSuccess = true;
				await this.OnAddSuccess.InvokeAsync(response);
			}

			this.isInProgress = false;
		}

		protected void OnDelete(ProductVatViewModel productVat)
		{
			this.RemoveProductToVat(productVat);
		}

		protected async Task OnCancel()
		{
			await this.OnCancelSuccess.InvokeAsync(null);
		}


		public void Dispose()
		{
		}

		private async Task AddProductToVatAsync()
		{
			if (AddOrEditViewModel.ProductVats.Select(x => x.Product.Code).Contains(this.productVatViewModel.Product?.Code)
				|| AddOrEditViewModel.ProductVats.Select(x => x.Product.Description).Contains(this.productVatViewModel.Product?.Description))
			{
				this.isAlreadyAdded = true;
			}
			else if (!string.IsNullOrWhiteSpace(this.productVatViewModel.Product?.Description)
				&& !string.IsNullOrWhiteSpace(this.productVatViewModel.Product?.Code)
				&& this.productVatViewModel.VatPercentage > 0
				&& this.productVatViewModel.Rate > 0
				&& this.productVatViewModel.Quantity > 0)
			{
				decimal taxableAmount = this.productVatViewModel.Amount - this.productVatViewModel.DiscountAmount;
				AddOrEditViewModel.VatPercentage = this.productVatViewModel.VatPercentage;
				AddOrEditViewModel.TotalTaxableAmount = AddOrEditViewModel.TotalTaxableAmount + this.productVatViewModel.TaxableAmount;
				AddOrEditViewModel.TotalVatAmount = AddOrEditViewModel.TotalVatAmount + this.productVatViewModel.TaxAmount;
				AddOrEditViewModel.TotalAmount = AddOrEditViewModel.TotalAmount + this.productVatViewModel.Amount;
				AddOrEditViewModel.ProductVats.Add(new ProductVatViewModel {
					Amount = this.productVatViewModel.Amount,
					DiscountAmount = this.productVatViewModel.DiscountAmount,
					Quantity = this.productVatViewModel.Quantity,
					QuantityType =  this.productVatViewModel.QuantityType,
					Rate =  this.productVatViewModel.Rate,
					TaxableAmount = this.productVatViewModel.TaxableAmount,
					TaxAmount = this.productVatViewModel.TaxAmount,
					VatPercentage = this.productVatViewModel.VatPercentage,
					Product = new ProductViewModel
					{
						Code = this.productVatViewModel.Product.Code,
						Description = this.productVatViewModel.Product.Description,
					}
				});
				this.invalidViewModel = false;
				this.isAlreadyAdded = false;
			}
			else
			{
				this.invalidViewModel = true;
				this.isAlreadyAdded = false;
			}

		}

		private void RemoveProductToVat(ProductVatViewModel productVat)
		{
			decimal taxableAmount = productVat.Amount - productVat.DiscountAmount;
			AddOrEditViewModel.VatPercentage = productVat.VatPercentage;
			AddOrEditViewModel.TotalTaxableAmount = AddOrEditViewModel.TotalTaxableAmount - productVat.TaxableAmount;
			AddOrEditViewModel.TotalVatAmount = AddOrEditViewModel.TotalVatAmount - productVat.TaxAmount;
			AddOrEditViewModel.TotalAmount = AddOrEditViewModel.TotalAmount - productVat.Amount;
			AddOrEditViewModel.ProductVats.RemoveAll(x => x.Product.Code == productVat.Product.Code);
			this.invalidViewModel = false;
		}
	}
}
