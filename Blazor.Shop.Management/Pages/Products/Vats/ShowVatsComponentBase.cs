﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Blazor.Shop.Management.Common.Constants;
using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;

namespace Blazor.Shop.Management.Pages.Products.Vats
{
	public class ShowVatsComponentBase : ComponentBase, IDisposable
	{
		private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
		
		[Inject]
		protected ShopManagementService shopService { get; set; }

		[Inject]
		protected NavigationManager navigationManager { get; set; }
		protected bool isInProgess { get; set; }
		protected bool isSuccess { get; set; }
		protected VatViewModel viewModel { get; set; }
		protected List<VatViewModel> viewModels { get; set; }

		protected override async Task OnInitializedAsync()
		{
			this.isInProgess = true;
			this.viewModels = await this.shopService.GetVatsAsync(cancellationTokenSource.Token);
			this.isInProgess = false;
		}

		protected void OnDtails(VatViewModel vat)
		{
			this.navigationManager.NavigateTo($"{LocalUrl.CalculateVatUrl}/{vat.InvoiceNumber}", true);
		}

		protected void OnCalculate()
		{
			this.viewModel = new VatViewModel();
		}

		protected void OnAddSuccess(VatViewModel vat)
		{
			if (viewModel != null)
			{
				this.viewModels.Insert(0, vat);
			}

			this.isInProgess = false;
		}

		protected void OnCancel()
		{
			this.viewModel = null;
		}

		public void Dispose()
		{
		}
	}
}
