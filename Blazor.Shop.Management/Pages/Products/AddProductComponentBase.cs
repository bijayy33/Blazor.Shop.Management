﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.Management.Common.Enums;
using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace Blazor.Shop.Management.Pages.Products
{
	public class AddProductComponentBase : ComponentBase, IDisposable
	{
		private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

		[Inject]
		protected ShopManagementService shopService { get; set; }

		[Inject]
		protected NavigationManager navigationManager { get; set; }

		[Parameter]
		public ProductViewModel viewModel { get; set; }

		[Parameter]
		public List<ProductViewModel> ViewModels { get; set; }

		[Parameter]
		public EventCallback<MouseEventArgs> OnCancel { get; set; }

		[Parameter]
		public EventCallback<ProductViewModel> OnAddSuccess { get; set; }
		protected bool isSuccess { get; set; }
		protected bool alreadyExists { get; set; }
		protected bool submitClicked { get; set; }
		protected bool isInProgress { get; set; }

		protected override async Task OnInitializedAsync()
		{
			this.isInProgress = true;
			this.viewModel = new ProductViewModel();

			this.isInProgress = false;
		}

		protected async Task OnSubmit()
		{
			if (this.ViewModels.Any(x => x.Code?.ToLower().Trim() == viewModel.Code?.ToLower().Trim()
				 || x.Description?.ToLower().Trim() == viewModel.Description?.ToLower().Trim()))
			{
				this.viewModel.Code = string.Empty;
				this.viewModel.Description = string.Empty;
				this.alreadyExists = true;
			}
			else
			{
				this.submitClicked = true;
				this.isInProgress = true;
				this.viewModel = await this.shopService.AddProductAsync(this.viewModel, cancellationTokenSource.Token);
				this.isInProgress = false;
				this.isSuccess = this.viewModel != null;

				if (this.viewModel != null)
				{
					this.viewModel.ActionType = ActionType.Add;
					await this.OnAddSuccess.InvokeAsync(this.viewModel);
				}
			}
		}

		protected void OnChange(string quantityType)
		{
			QuantityType type;

			Enum.TryParse(quantityType, out type);
			this.viewModel.QuantityType = type;

			if (type == QuantityType.PCS)
			{
				this.viewModel.QuantityTypeValue = 1;
			}
			else
			{
				this.viewModel.QuantityTypeValue = 2;
			}
		}

		private async Task Update(ProductViewModel viewModel)
		{
			this.isInProgress = true;

				this.isSuccess = await this.shopService.UpdateProductAsync(viewModel, cancellationTokenSource.Token) != null;

				if (this.isSuccess)
				{}

				this.isInProgress = false;
			}

		protected void Cancel(MouseEventArgs args)
		{
			this.OnCancel.InvokeAsync(args);
		}

		public void Dispose()
		{
		}
	}
}
