﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.Management.Common.Enums;
using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace Blazor.Shop.Management.Pages.Products
{
	public class ProductsComponentBase : ComponentBase, IDisposable
	{
		private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

		[Inject]
		protected ShopManagementService shopService { get; set; }

		protected ProductViewModel selectedViewModel { get; set; }
		protected bool isInProgress { get; set; }
		protected bool isSuccess { get; set; }
		protected bool isDelete { get; set; }
		protected List<ProductViewModel> viewModels { get; set; }

		protected async override Task OnInitializedAsync()
		{
			this.isInProgress = true;
			this.viewModels = await this.shopService.GetProductsAsync(cancellationTokenSource.Token) as List<ProductViewModel>;
			this.isInProgress = false;
		}

		protected void OnAdd()
		{
			this.selectedViewModel = new ProductViewModel();
		}

		protected void OnCancel(MouseEventArgs eventArgs)
		{
			this.OnResponse();
		}

		protected void OnEdit(ProductViewModel viewModel)
		{
			this.selectedViewModel = viewModel;
		}

		protected void OnDelete(ProductViewModel viewModel)
		{
			this.isDelete = true;
		}

		protected async Task OnRestore(ProductViewModel viewModel)
		{
			this.isInProgress = true;
			viewModel.IsDeleted = false;
			viewModel = await this.shopService.UpdateProductAsync(viewModel, cancellationTokenSource.Token);

			this.OnResponse(new { Action = ActionType.Edit, ViewModel = viewModel });
		}

		protected async void OnYes(ProductViewModel viewModel)
		{
			this.isInProgress = true;
			viewModel = await this.shopService.DeleteProductAsync(viewModel.Id, cancellationTokenSource.Token);

			this.OnResponse(new { Action = ActionType.Delete, ViewModel = viewModel });
		}

		protected void OnNo()
		{
			this.OnResponse();
		}

		/// <summary>
		/// TO DO - load 50 products at a time.
		/// </summary>
		/// <returns></returns>
		protected async Task OnLoadMoreAsync()
		{
			var products = await this.shopService.GetProductsAsync();

			if (products != null && products.Any())
			{
				this.viewModels.AddRange(products);
			}
		}

		protected void OnResponse(dynamic result = null)
		{
			ProductViewModel viewModel = (ProductViewModel)result?.ViewModel;

			if (viewModel != null)
			{
				switch (result.Action)
				{
					case ActionType.Add:
						this.viewModels.Insert(0, viewModel);
						this.selectedViewModel = null;

						break;
					case ActionType.Edit:
						int index = this.viewModels.FindIndex(x => x.Id == viewModel.Id);
						this.viewModels[index] = viewModel;
						this.selectedViewModel = null;

						break;
					case ActionType.Delete:
						this.viewModels.RemoveAll(x => x.Id == viewModel.Id);
						this.selectedViewModel = null;

						break;
					default:
						break;
				}
			}

			this.isDelete = false;
			this.isInProgress = false;
		}

		public void Dispose()
		{
			this.OnResponse();
			this.isSuccess = false;
			this.viewModels = null;
		}
	}
}
