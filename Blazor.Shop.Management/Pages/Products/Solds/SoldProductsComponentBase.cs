﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.Management.Common.Constants;
using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;

namespace Blazor.Shop.Management.Pages.Products.Solds
{
	public class SoldProductsComponentBase : ComponentBase, IDisposable
	{
		private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

		[Inject]
		protected ShopManagementService shopService { get; set; }

		[Inject]
		protected NavigationManager navigationManager { get; set; }
		protected bool isInProgress { get; set; }
		protected bool isSuccess { get; set; } = true;
		protected List<SoldViewModel> viewModels { get; set; }

		protected override async Task OnInitializedAsync()
		{
			this.isInProgress = true;

			this.viewModels = await this.shopService.GetSoldsAsync(cancellationTokenSource.Token);
			this.isInProgress = false;
		}

		protected void OnDtails(SoldViewModel sold)
		{
			this.navigationManager.NavigateTo($"{LocalUrl.AddSoldProductUrl}/{sold.Id}", true);
		}

		protected async Task OnDeleteAsync(SoldViewModel sold)
		{
			this.isInProgress = true;

			var deletedSold = await this.shopService.DeleteSoldById(sold.Id);
			this.isSuccess = deletedSold != null;

			if (deletedSold != null)
			{
				this.viewModels.RemoveAll(x => x.Id == sold.Id);
			}

			this.isInProgress = false;
		}

		protected void OnCancel()
		{
			this.navigationManager.NavigateTo(LocalUrl.AddSoldProductUrl, true);
		}

		public void Dispose()
		{
		}
	}
}