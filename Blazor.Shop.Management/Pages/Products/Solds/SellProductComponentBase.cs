﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.Management.Common.Constants;
using Blazor.Shop.Management.Common.Enums;
using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;

namespace Blazor.Shop.Management.Pages.Products.Solds
{
	public class SellProductComponentBase : ComponentBase, IDisposable
	{
		private CancellationTokenSource CancellationTokenSource = new CancellationTokenSource();

		[Parameter]
		public string Redirect { get; set; }

		[Inject]
		protected ShopManagementService shopService { get; set; }

		[Inject]
		protected UserManagementService userManagementService { get; set; }

		[Inject]
		protected NavigationManager navigationManager { get; set; }
		protected EditContext editContext { get; set; }
		protected ValidationMessageStore messageStore { get; set; }
		protected List<ProductViewModel> productViewModels { get; set; }
		protected SoldViewModel AddOrEditSoldViewModel { get; set; }
		protected SoldProductViewModel soldProductViewModel { get; set; }
		protected List<SoldViewModel> alreadyAddedSolds { get; set; }
		protected List<SoldProductViewModel> viewModels { get; set; }
		protected List<UserViewModel> users { get; set; } = new List<UserViewModel>();
		protected string buyerPhone { get; set; } = string.Empty;
		protected string sellerPhone { get; set; } = string.Empty;
		protected bool isCode { get; set; }
		protected bool isDescription { get; set; }
		protected bool isInit { get; set; } = true;
		protected bool isGuid { get; set; }
		protected bool isShowLog { get; set; }
		protected bool isEdit { get; set; }
		protected bool isInProgress { get; set; }
		protected bool isAlreadyAdded { get; set; }
		protected bool invalidViewModel { get; set; }
		protected bool isSuccess { get; set; }
		protected bool submitClicked { get; set; }

		protected async override Task OnInitializedAsync()
		{
			this.isInProgress = true;
			this.soldProductViewModel = new SoldProductViewModel();
			this.viewModels = new List<SoldProductViewModel>();
			this.editContext = new EditContext(this.soldProductViewModel);
			this.messageStore = new ValidationMessageStore(this.editContext);
			editContext.OnValidationRequested += (s, e) => this.messageStore.Clear();
			editContext.OnFieldChanged += (s, e) => this.messageStore.Clear(e.FieldIdentifier);

			Guid guid;
			this.Redirect = this.Redirect?.Trim();
			isGuid = Guid.TryParse(this.Redirect, out guid);
			this.alreadyAddedSolds = await this.shopService.GetSoldsAsync() ?? new List<SoldViewModel>();

			if (string.IsNullOrEmpty(this.Redirect))
			{
				this.productViewModels = await this.shopService.GetProductsAsync() ?? new List<ProductViewModel>();
				this.users = (await this.userManagementService.GetUsersAsync()) ?? new List<UserViewModel>();
			}
			else if (isGuid)
			{
				var soldViewModel = await this.shopService.GetSoldByIdAsync(guid.ToString());
				this.AddOrEditSoldViewModel = soldViewModel;
			}
			this.isInProgress = false;
		}

		protected void OnSubmit()
		{
			this.isInit = false;
			this.submitClicked = false;

			if (this.editContext.Validate())
			{
				if (this.AddOrEditSoldViewModel.SoldProducts == null)
				{
					this.AddOrEditSoldViewModel.SoldProducts = new List<SoldProductViewModel>();
					this.AddProductToSold();
				}
				else if (this.AddOrEditSoldViewModel.SoldProducts.Count() >= 0)
				{
					this.AddProductToSold();
				}
			}
		}

		protected async Task OnUpdateAsync()
		{
			this.isInit = false;
			this.isEdit = true;
			this.isInProgress = true;
			var response = await this.shopService.UpdateSoldProductAsync(this.AddOrEditSoldViewModel, CancellationTokenSource.Token);

			if (response == null)
			{
				this.submitClicked = true;
				this.isSuccess = false;
			}
			else
			{
				this.isSuccess = true;
				await Task.Run(() => this.navigationManager.NavigateTo(LocalUrl.SoldProductListUrl, true));
			}

			this.isInProgress = false;
		}

		protected async Task OnPrintAsync()
		{
			this.isInit = false;
			this.isEdit = false;

			if (!this.AddOrEditSoldViewModel.SoldProducts.Any())
			{
				this.navigationManager.NavigateTo(LocalUrl.AddSoldProductUrl, true);
			}
			else
			{
				if (this.alreadyAddedSolds.Any(x => x.CreationDate.Value.DateTime.ToShortDateString() == DateTime.Now.ToShortDateString() && x.Buyer != null && x.Buyer.PhoneNumber != null && x.Buyer.PhoneNumber.Trim().Contains(this.buyerPhone?.Trim()) && x.SoldProducts.Any(y => y.Product?.Code?.Trim().ToLower() == this.soldProductViewModel.Product.Code?.ToLower().Trim())))
				{
					this.soldProductViewModel.Product.Code = string.Empty;
					this.soldProductViewModel.Product.Description = string.Empty;
					return;
				}

				this.isInProgress = true;
				var response = await this.shopService.AddSoldProductAsync(this.AddOrEditSoldViewModel, CancellationTokenSource.Token);

				if (response == null)
				{
					this.submitClicked = true;
					this.isSuccess = false;
				}
				else
				{
					this.isSuccess = true;
					this.alreadyAddedSolds.Insert(0, response);
				}

				this.isInProgress = false;
			}
		}

		protected async Task OnDelete(SoldProductViewModel soldProduct)
		{
			this.isInit = false;
			this.isEdit = false;
			if (this.isGuid)
			{
				this.isInProgress = true;
				soldProduct = await this.shopService.DeleteSoldProductById(soldProduct.Id);
				this.isInProgress = false;
			}

			this.RemoveProductFromSold(soldProduct);
		}

		protected void OnToggleLog()
		{
			this.isShowLog = !this.isShowLog;
		}

		protected void IsCode()
		{
			this.isCode = true;
			this.isDescription = false;
			var product = this.productViewModels.FirstOrDefault(x => x.Code == this.soldProductViewModel.Product.Code);
			this.soldProductViewModel.AvailableQuantity = product == null ? 0 : product.AvailableQuantity;
			this.soldProductViewModel.Product.Description = product?.Description;
			this.soldProductViewModel.Rate = product == null ? 0 : product.SellingPrice;
			this.soldProductViewModel.QuantityType = product == null ? QuantityType.PCS : product.QuantityType;

			if (product?.AvailableQuantity < this.soldProductViewModel.SoldQuantity)
			{
				this.soldProductViewModel.SoldQuantity = 0;
			}

			if (product == null)
			{
				this.isCode = false;
			}
		}

		protected void IsDescription()
		{
			this.isDescription = true;
			this.isCode = false;
			var product = this.productViewModels.FirstOrDefault(x => x.Description == this.soldProductViewModel.Product.Description);
			this.soldProductViewModel.AvailableQuantity = product == null ? 0 : product.AvailableQuantity;
			this.soldProductViewModel.Product.Code = product?.Code;
			this.soldProductViewModel.Rate = product == null ? 0 : product.SellingPrice;
			this.soldProductViewModel.QuantityType = product == null ? QuantityType.PCS : product.QuantityType;

			if (product?.AvailableQuantity < this.soldProductViewModel.SoldQuantity)
			{
				this.soldProductViewModel.SoldQuantity = 0;
			}

			if (product == null)
			{
				this.isDescription = false;
			}
		}

		protected void OnSoldQuantity(bool isNew = false)
		{
			if (isNew)
			{
				var product = this.productViewModels.FirstOrDefault(x => x.Description == this.soldProductViewModel.Product.Description);

				if (product?.AvailableQuantity < this.soldProductViewModel.SoldQuantity)
				{
					this.soldProductViewModel.SoldQuantity = 0;
				}

				this.soldProductViewModel.Product.AvailableQuantity = product == null ? 0 : product.AvailableQuantity - this.soldProductViewModel.SoldQuantity;
			}
			else
			{
				this.isInit = false;
				for (int i = 0; i < this.AddOrEditSoldViewModel.SoldProducts.Count(); i++)
				{
					if (this.AddOrEditSoldViewModel.SoldProducts[i].AvailableQuantity < this.AddOrEditSoldViewModel.SoldProducts[i].SoldQuantity)
					{
						this.AddOrEditSoldViewModel.SoldProducts[i].SoldQuantity = this.AddOrEditSoldViewModel.SoldProducts[i].AvailableQuantity;
					}

					if (this.AddOrEditSoldViewModel.SoldProducts[i].AvailableQuantity <= 0)
					{
						this.AddOrEditSoldViewModel.SoldProducts.RemoveAll(x => x.Product.Code == this.AddOrEditSoldViewModel.SoldProducts[i].Product.Code);
					}
					else if (this.AddOrEditSoldViewModel.SoldProducts[i].SoldQuantity <= 0)
					{
						this.AddOrEditSoldViewModel.SoldProducts[i].SoldQuantity = 1;
					}

					if (this.AddOrEditSoldViewModel.SoldProducts[i].AvailableQuantity < this.AddOrEditSoldViewModel.SoldProducts[i].SoldQuantity)
					{
						this.AddOrEditSoldViewModel.SoldProducts[i].SoldQuantity = this.AddOrEditSoldViewModel.SoldProducts[i].AvailableQuantity;
					}
				}
			}
		}

		protected void OnTotalDiscountModified()
		{
			this.isInit = false;
			for (int i = 0; i < this.AddOrEditSoldViewModel.SoldProducts.Count(); i++)
			{
				this.AddOrEditSoldViewModel.SoldProducts[i].DiscountAmount = 0;
			}

			decimal totalAmount = this.AddOrEditSoldViewModel.SoldProducts.Sum(x => x.Amount);
			this.AddOrEditSoldViewModel.TotalAmount = totalAmount - this.AddOrEditSoldViewModel.TotalDiscountAmount;
		}

		protected void OnTotalPaidModified() => this.isInit = false;

		protected void OnCancel()
		{
			this.navigationManager.NavigateTo(LocalUrl.SoldProductListUrl);
		}


		public void Dispose()
		{
		}

		private void AddProductToSold()
		{
			this.invalidViewModel = true;
			this.isAlreadyAdded = false;

			if (this.AddOrEditSoldViewModel.SoldProducts.Select(x => x.Product.Code).Contains(this.soldProductViewModel.Product?.Code)
				|| this.AddOrEditSoldViewModel.SoldProducts.Select(x => x.Product.Description).Contains(this.soldProductViewModel.Product?.Description))
			{
				this.isAlreadyAdded = true;
			}
			else if (!string.IsNullOrWhiteSpace(this.soldProductViewModel.Product?.Description)
				&& !string.IsNullOrWhiteSpace(this.soldProductViewModel.Product?.Code)
				&& this.soldProductViewModel.Rate > 0
				&& this.soldProductViewModel.DiscountAmount >= 0
				&& this.soldProductViewModel.Amount > 0
				&& this.soldProductViewModel.AvailableQuantity > 0
				&& this.soldProductViewModel.SoldQuantity > 0
				&& this.soldProductViewModel.AvailableQuantity >= this.soldProductViewModel.SoldQuantity)
			{
				decimal totalAmountToPay = soldProductViewModel.SoldQuantity * (soldProductViewModel.Rate - soldProductViewModel.DiscountAmount);
				this.AddOrEditSoldViewModel.TotalAmount = this.AddOrEditSoldViewModel.TotalAmount + totalAmountToPay;
				this.AddOrEditSoldViewModel.TotalDiscountAmount = this.AddOrEditSoldViewModel.TotalDiscountAmount + this.soldProductViewModel.SoldQuantity * this.soldProductViewModel.DiscountAmount;

				this.AddOrEditSoldViewModel.Buyer = this.users.FirstOrDefault(x => x.PhoneNumber.Contains(buyerPhone));
				this.AddOrEditSoldViewModel.BuyerId = this.AddOrEditSoldViewModel.Buyer?.Id;
				this.AddOrEditSoldViewModel.Seller = this.users.FirstOrDefault(x => x.PhoneNumber.Contains(sellerPhone));
				this.AddOrEditSoldViewModel.SellerId = this.AddOrEditSoldViewModel.Seller?.Id;

				this.AddOrEditSoldViewModel.SoldProducts.Add(new SoldProductViewModel
				{
					Rate = this.soldProductViewModel.Rate,
					Amount = this.soldProductViewModel.Amount,
					DiscountAmount = this.soldProductViewModel.DiscountAmount,
					AvailableQuantity = this.soldProductViewModel.AvailableQuantity,
					QuantityType = this.soldProductViewModel.QuantityType,
					SoldQuantity = this.soldProductViewModel.SoldQuantity,
					Product = new ProductViewModel
					{
						Code = this.soldProductViewModel.Product.Code,
						Description = this.soldProductViewModel.Product.Description,
						AvailableQuantity = this.soldProductViewModel.Product.AvailableQuantity,
					}
				});

				this.invalidViewModel = false;
				this.isAlreadyAdded = false;
			}
			else if (this.soldProductViewModel.AvailableQuantity < this.soldProductViewModel.SoldQuantity)
			{
				this.soldProductViewModel.SoldQuantity = 0;
			}
		}

		private void RemoveProductFromSold(SoldProductViewModel soldProduct)
		{
			if (soldProduct != null)
			{
				decimal totalAmountToPay = soldProduct.SoldQuantity * (soldProduct.Rate - soldProduct.DiscountAmount);
				this.AddOrEditSoldViewModel.TotalAmount = this.AddOrEditSoldViewModel.TotalAmount - totalAmountToPay;
				this.AddOrEditSoldViewModel.TotalDiscountAmount = this.AddOrEditSoldViewModel.TotalDiscountAmount - (soldProduct.SoldQuantity * soldProduct.DiscountAmount);
				this.AddOrEditSoldViewModel.SoldProducts.RemoveAll(x => x.Product?.Code == soldProduct.Product?.Code);
				this.invalidViewModel = false;
			}
		}
	}
}
