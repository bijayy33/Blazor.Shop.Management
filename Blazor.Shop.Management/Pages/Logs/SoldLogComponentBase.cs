﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Blazor.Shop.Management.Common.Constants;
using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;

namespace Blazor.Shop.Management.Pages.Logs
{
	public class SoldLogComponentBase : ComponentBase
	{
		[Inject]
		protected ShopManagementService shopService { get; set; }

		[Inject]
		protected NavigationManager navigationManager { get; set; }

		[Parameter]
		public string Redirect { get; set; }

		[Parameter]
		public List<SoldLogViewModel> ViewModels { get; set; }
		
		[Parameter]
		public EventCallback<SoldLogViewModel> OnDeleteCallback { get; set; }

		protected bool isInProgress { get; set; }
		protected bool isSuccess { get; set; } = true;

		protected override async Task OnInitializedAsync()
		{
			this.isInProgress = true;

			if (!string.IsNullOrWhiteSpace(this.Redirect))
			{
				this.ViewModels = await this.shopService.GetLogsBySoldId(this.Redirect.Trim());
			}

			this.isInProgress = false;
		}

		protected async Task OnDeleteAsync(SoldLogViewModel log)
		{
			this.isInProgress = true;

			var deletedLog = await this.shopService.DeleteSoldLogById(log.Id);
			this.isSuccess = deletedLog != null;

			if (deletedLog != null)
			{
				this.ViewModels.RemoveAll(x => x.Id == log.Id);
			}

			this.isInProgress = false;
		}
	}
}
