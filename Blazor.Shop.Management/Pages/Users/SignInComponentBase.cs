﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.Management.Common.Constants;
using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace Blazor.Shop.Management.Pages.Users
{
	public class SignInComponentBase : ComponentBase, IDisposable
	{
		private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

		[Inject]
		protected ShopManagementService shopService { get; set; }

		[Inject]
		protected UserManagementService userService { get; set; }

		[Inject]
		protected NavigationManager navigationManager { get; set; }

		protected SignInViewModel ViewModel { get; set; }
		protected bool IsInit { get; set; } = true;
		protected bool IsSuccess { get; set; }
		protected bool isInProgress { get; set; } = true;

		protected async override Task OnInitializedAsync()
		{
			this.ViewModel = new SignInViewModel();
			this.isInProgress = false;
			await base.OnInitializedAsync();
		}

		protected async Task OnLogInAsync()
		{
			this.isInProgress = true;
			var response = await this.userService.LogInAsync(ViewModel, cancellationTokenSource.Token);
			this.IsSuccess = response.IsSuccess;

			if (this.IsSuccess)
			{
				this.navigationManager.NavigateTo($"{LocalUrl.ProductListUrl}{LocalUrl.RedirectString}", true);
			}

			this.IsInit = false;
			this.isInProgress = false;
		}

		public void Dispose()
		{
		}
	}
}
