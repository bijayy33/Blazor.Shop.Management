﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.Management.Common.Enums;
using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;

namespace Blazor.Shop.Management.Pages.Users
{
	public class SignUpComponentBase : ComponentBase
	{
		private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

		[Inject]
		protected UserManagementService UserService { get; set; }

		[Parameter]
		public UserViewModel ViewModel { get; set; }

		[Parameter]
		public List<UserViewModel> ViewModels { get; set; }

		[Parameter]
		public EventCallback<UserViewModel> OnResponse { get; set; }

		[Parameter]
		public EventCallback OnCancelClicked { get; set; }
		protected bool IsServerError { get; set; }
		protected bool IsSuccess { get; set; }
		protected bool IsUpdateFailed { get; set; }
		protected bool IsExists { get; set; }
		protected bool IsInProgress { get; set; } = true;

		protected override Task OnInitializedAsync()
		{
			this.ViewModel = this.ViewModel ?? new UserViewModel();
			this.IsInProgress = false;
			return base.OnInitializedAsync();
		}

		protected async Task SignUpClickedAsync()
		{
			this.IsExists = false;
			this.IsInProgress = true;
			this.IsUpdateFailed = false;

			if(!string.IsNullOrWhiteSpace(this.ViewModel.Id))
			{
				var response = await this.UserService.UpdateUserAsync(ViewModel, cancellationTokenSource.Token);
				this.IsSuccess = response.IsSuccess;
				this.IsUpdateFailed = true;

				if (this.IsSuccess)
				{
					response.Data.ActionType = ActionType.Edit;
					await this.OnResponse.InvokeAsync(response.Data);
				}
			}
			else if (this.ViewModels.Any(x => x.PhoneNumber.Contains(this.ViewModel.PhoneNumber)))
			{
				this.IsExists = true;
			}
			else
			{
				var response = await this.UserService.SignUpUserAsync(ViewModel, cancellationTokenSource.Token);
				this.IsSuccess = response.IsSuccess;

				if (this.IsSuccess)
				{
					response.Data.ActionType = ActionType.Add;
					await this.OnResponse.InvokeAsync(response.Data);
				}
			}

			this.IsInProgress = false;
		}

		protected void OnRoleChanged(ChangeEventArgs args)
		{
			this.ViewModel.Role = args.Value.ToString();
		}
	}
}
