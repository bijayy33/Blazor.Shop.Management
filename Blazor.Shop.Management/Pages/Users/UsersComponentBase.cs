﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.Management.Common.Enums;
using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;

namespace Blazor.Shop.Management.Pages.Users
{
	public class UsersComponentBase : ComponentBase, IDisposable
	{
		private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

		[Inject]
		protected UserManagementService UserService { get; set; }

		[Inject]
		protected AppStateService AppStateService { get; set; }
		protected List<UserViewModel> ViewModels { get; set; }
		protected List<UserViewModel> FilteredViewModels { get; set; }
		protected UserViewModel SelectedViewModel { get; set; }
		protected string SearchText { get; set; }
		protected string SearchColumnName { get; set; }
		protected bool IsSuccess { get; set; }
		protected bool IsInProgress { get; set; } = true;

		protected override async Task OnInitializedAsync()
		{
			this.AppStateService.OnChange += this.StateHasChanged;

			var response = await this.UserService.GetUsersAsync(cancellationTokenSource.Token);

			if (response.Data != null)
			{
				this.FilteredViewModels = response.Data;
				this.ViewModels = response.Data;
			}

			this.IsInProgress = false;
		}

		protected void AddOrEditClicked(UserViewModel user)
		{
			if (user == null)
			{
				this.SelectedViewModel = new UserViewModel();
			}

			this.SelectedViewModel = user;
		}

		protected void OnCancelClicked()
		{
			this.AppStateService.IsCancelClicked = true;
		}

		protected void OnSearchInput(ChangeEventArgs args, string columnName)
		{
			this.SearchText = args.Value.ToString().Trim().ToLower();
			this.SearchColumnName = columnName;

			if (this.ViewModels != null && this.SearchText.Length > 2)
			{
				switch (columnName)
				{
					case nameof(UserViewModel.FullName):
						this.FilteredViewModels = this.ViewModels.Where(x => x.FullName.ToLower().Contains(this.SearchText)).ToList();
						break;

					case nameof(UserViewModel.Email):
						this.FilteredViewModels = this.ViewModels.Where(x => x.Email != null && x.Email.ToLower().Contains(this.SearchText)).ToList();
						break;

					case nameof(UserViewModel.PhoneNumber):
						this.FilteredViewModels = this.ViewModels.Where(x => x.PhoneNumber.ToLower().Contains(this.SearchText)).ToList();
						break;

					case nameof(UserViewModel.Address):
						this.FilteredViewModels = this.ViewModels.Where(x => x.Address.ToLower().Contains(this.SearchText)).ToList();
						break;

					default: //Roles
						this.FilteredViewModels = this.ViewModels.Where(x => x.Role.ToLower().Contains(this.SearchText)).ToList();
						break;
				}
			}
			else if(this.SearchText == string.Empty)
			{
				this.FilteredViewModels = this.ViewModels;
			}

			this.AppStateService.IsSearchMore = false;

			if (this.FilteredViewModels.Count == 0)
			{
				this.AppStateService.IsSearchMore = true;
			}
		}

		protected async Task OnSearchMoreInputAsync()
		{
			this.IsInProgress = true;
			if(!string.IsNullOrWhiteSpace(this.SearchText) && !string.IsNullOrWhiteSpace(this.SearchColumnName))
			{
				this.FilteredViewModels = await this.UserService.SearchUsersByColumNameAsync(this.SearchColumnName, this.SearchText);
			}

			this.IsSuccess = this.FilteredViewModels != null;
			this.IsInProgress = false;
		}

		protected async Task OnResponse(UserViewModel viewModel = null)
		{
			this.IsInProgress = true;

			if (viewModel != null)
			{
				switch (viewModel.ActionType)
				{
					case ActionType.Add:
						this.ViewModels.Insert(0, viewModel);
						break;

					case ActionType.Edit:
						int index = this.ViewModels.FindIndex(x => x.Id == viewModel.Id);
						this.ViewModels[index] = viewModel;
						break;

					case ActionType.Delete:
						var response = await this.UserService.DeleteUserAsync(viewModel.Id, cancellationTokenSource.Token);
						this.IsSuccess = response.IsSuccess;

						if (this.IsSuccess)
						{
							this.ViewModels.RemoveAll(x => x.Id == viewModel.Id);
						}
						break;

					case ActionType.Restore:
						viewModel.IsDeleted = false;
						response = await this.UserService.UpdateUserAsync(viewModel, cancellationTokenSource.Token);
						this.IsSuccess = response.IsSuccess;

						if (this.IsSuccess)
						{
							index = this.ViewModels.FindIndex(x => x.Id == response.Data.Id);
							this.ViewModels[index] = response.Data;
						}
						break;

					default: //Load More
						var users = await this.UserService.GetUsersByPageAsync(this.ViewModels.Count);

						if(users != null)
						{
							this.ViewModels.AddRange(users);
						}
						break;
				}
			}

			this.IsInProgress = false;
		}

		public void Dispose()
		{
		}
	}
}
