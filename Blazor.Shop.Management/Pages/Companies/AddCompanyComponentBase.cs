﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Blazor.Shop.Management.Common.Enums;
using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Web;

namespace Blazor.Shop.Management.Pages.Companies
{
	public class AddCompanyComponentBase : ComponentBase, IDisposable
	{
		[Inject]
		protected ShopManagementService shopService { get; set; }

		[Parameter]
		public CompanyViewModel ViewModel { get; set; }

		[Parameter]
		public List<CompanyViewModel> ViewModels { get; set; }

		[Parameter]
		public EventCallback<MouseEventArgs> OnCancel { get; set; }

		[Parameter]
		public EventCallback<CompanyViewModel> OnAddSuccess { get; set; }
		public EditContext editContext { get; set; }
		protected bool isSuccess { get; set; }
		protected bool alreadyExists { get; set; }
		protected bool submitClicked { get; set; }
		protected bool isInProgress { get; set; }

		protected override async Task OnInitializedAsync()
		{
			this.isInProgress = true;
			this.ViewModel = new CompanyViewModel();
			this.editContext = new EditContext(this.ViewModel);
			this.isInProgress = false;
			await base.OnInitializedAsync();
		}

		protected async Task OnSubmitAsync()
		{
			this.alreadyExists = false;

			if (this.ViewModels.Any(x => x.Name == this.ViewModel.Name
									|| x.TaxTypeNumber == this.ViewModel.TaxTypeNumber))
			{
				this.alreadyExists = true;
				return;
			}

			this.submitClicked = true;
			if (this.editContext.Validate())
			{
				this.isInProgress = true;
				this.ViewModel = await this.shopService.AddCompany(this.ViewModel);
				this.isInProgress = false;
				this.isSuccess = this.ViewModel != null;

				if (this.ViewModel != null)
				{
					await this.OnAddSuccess.InvokeAsync(this.ViewModel);
				}

				this.ViewModel = null;
			}
		}

		protected void OnChangeTaxType(string taxType)
		{
			TaxType type;

			Enum.TryParse(taxType, out type);
			this.ViewModel.TaxType = type;
		}

		protected void OnChangeCompanyType(string companyType)
		{
			CompanyType type;

			Enum.TryParse(companyType, out type);
			this.ViewModel.Type = type;
		}

		protected void Cancel(MouseEventArgs args)
		{
			this.OnCancel.InvokeAsync(args);
		}

		public void Dispose()
		{
		}
	}
}
