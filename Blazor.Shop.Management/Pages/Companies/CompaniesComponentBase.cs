﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Blazor.Shop.Management.Common.Constants;
using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;

namespace Blazor.Shop.Management.Pages.Companies
{
	public class CompaniesComponentBase : ComponentBase, IDisposable
	{
		[Inject]
		protected ShopManagementService shopService { get; set; }

		[Inject]
		protected NavigationManager navigationManager { get; set; }
		protected bool isInProgess { get; set; }
		protected bool isSuccess { get; set; }
		protected CompanyViewModel viewModel { get; set; }
		protected CompanyViewModel selectedViewModel { get; set; }
		protected List<CompanyViewModel> viewModels { get; set; }

		protected override async Task OnInitializedAsync()
		{
			this.isInProgess = true;

			this.selectedViewModel = new CompanyViewModel();
			this.viewModels = await this.shopService.GetCompanies();
			this.isInProgess = false;
		}

		protected void OnAdd()
		{
			this.viewModel = new CompanyViewModel();
		}

		protected void OnAddSuccess(CompanyViewModel company)
		{
			this.isSuccess = false;
			this.isInProgess = false;

			if (company != null)
			{
				this.viewModels.Insert(0, company);
				this.isSuccess = true;
				company = null;
			}
		}

		protected void OnCancel()
		{
			this.viewModel = null;
		}

		protected void OnDelete(CompanyViewModel company)
		{
			this.selectedViewModel = company;
		}

		protected async Task OnYesAsync(CompanyViewModel company)
		{
			this.isInProgess = true;
			var deletedCompany = await this.shopService.DeleteCompanyById(company.Id);

			if(deletedCompany != null)
			{
				this.viewModels.RemoveAll(x => x.Id == deletedCompany.Id);
			}

			this.isInProgess = false;
		}

		protected void OnNo()
		{
			this.selectedViewModel = new CompanyViewModel();
		}

		public void Dispose()
		{
		}
	}
}