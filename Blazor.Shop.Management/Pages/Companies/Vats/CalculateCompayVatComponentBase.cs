﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Blazor.Shop.Management.Common.Enums;
using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Web;

namespace Blazor.Shop.Management.Pages.Companies.Vats
{
	public class CalculateCompayVatComponentBase : ComponentBase, IDisposable
	{
		[Inject]
		protected ShopManagementService shopService { get; set; }

		[Parameter]
		public CompanyVatViewModel ViewModel { get; set; }

		[Parameter]
		public List<CompanyVatViewModel> ViewModels { get; set; }

		[Parameter]
		public EventCallback<MouseEventArgs> OnCancel { get; set; }

		[Parameter]
		public EventCallback<CompanyVatViewModel> OnAddSuccess { get; set; }
		public EditContext editContext { get; set; }
		public List<CompanyViewModel> companyViewModels { get; set; }
		protected bool isSuccess { get; set; }
		protected bool alreadyExists { get; set; }
		protected bool isVatOrPan { get; set; }
		protected bool isCompanyName { get; set; }
		protected bool submitClicked { get; set; }
		protected bool isInProgress { get; set; }

		protected override async Task OnInitializedAsync()
		{
			this.isInProgress = true;
			this.ViewModel = new CompanyVatViewModel();
			this.editContext = new EditContext(this.ViewModel);

			this.companyViewModels = await this.shopService.GetCompanies();
			this.isInProgress = false;
			await base.OnInitializedAsync();
		}

		protected async Task OnSubmitAsync()
		{
			this.alreadyExists = false;

			if (this.ViewModels.Any(x => (x.Company.Name == this.ViewModel.Company.Name
									|| x.Company.TaxTypeNumber == this.ViewModel.Company.TaxTypeNumber)
									&& x.BillNumber == this.ViewModel.BillNumber))
			{
				this.alreadyExists = true;
				return;
			}
			
			this.submitClicked = true;
			if (this.editContext.Validate())
			{
				this.isInProgress = true;
				this.ViewModel = await this.shopService.AddCompanyVat(this.ViewModel);
				this.isInProgress = false;
				this.isSuccess = this.ViewModel != null;

				if (this.ViewModel != null)
				{
					await this.OnAddSuccess.InvokeAsync(this.ViewModel);
				}

				this.ViewModel = null;
			}
		}

		protected void Cancel(MouseEventArgs args)
		{
			this.OnCancel.InvokeAsync(args);
		}

		protected void IsCompanyName()
		{
			this.isCompanyName = true;
			this.isVatOrPan = false;
			var company = this.companyViewModels.FirstOrDefault(x => x.Name == this.ViewModel.Company.Name);
			this.ViewModel.Company.TaxTypeNumber = company?.TaxTypeNumber;
			this.ViewModel.Company.TaxType = company == null ? TaxType.PAN : company.TaxType;

			if (company == null)
				this.isCompanyName = false;
		}

		protected void IsVatOrPan()
		{
			this.isCompanyName = false;
			this.isVatOrPan = true;
			var company = this.companyViewModels.FirstOrDefault(x => x.TaxTypeNumber == this.ViewModel.Company.TaxTypeNumber);
			this.ViewModel.Company.Name = company?.Name;
			this.ViewModel.Company.TaxType = company == null ? TaxType.PAN : company.TaxType;

			if (company == null)
				this.isVatOrPan = false;
		}

		public void Dispose()
		{
		}
	}
}