﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;

namespace Blazor.Shop.Management.Pages.Companies.Vats
{
	public class CompanyVatsComponentBase : ComponentBase, IDisposable
	{
		[Inject]
		protected ShopManagementService shopService { get; set; }

		[Inject]
		protected NavigationManager navigationManager { get; set; }
		protected bool isInProgess { get; set; }
		protected bool isSuccess { get; set; }
		protected CompanyVatViewModel viewModel { get; set; }
		public CompanyVatViewModel selectedViewModel { get; set; }
		protected List<CompanyVatViewModel> viewModels { get; set; }

		protected override async Task OnInitializedAsync()
		{
			this.isInProgess = true;

			this.selectedViewModel = new CompanyVatViewModel();
			this.viewModels = await this.shopService.GetCompanyVats();
			this.isInProgess = false;
		}

		protected void OnAdd()
		{
			this.viewModel = new CompanyVatViewModel();
		}

		protected void OnAddSuccess(CompanyVatViewModel companyVat)
		{
			this.isSuccess = false;
			this.isInProgess = false;

			if (companyVat != null)
			{
				this.viewModels.Insert(0, companyVat);
				this.isSuccess = true;
				companyVat = null;
			}
		}

		protected void OnCancel()
		{
			this.viewModel = null;
		}

		protected void OnDelete(CompanyVatViewModel company)
		{
			this.selectedViewModel = company;
		}

		protected async Task OnYesAsync(CompanyVatViewModel company)
		{
			this.isInProgess = true;
			var deletedCompanyVat = await this.shopService.DeleteCompanyVatById(company.Id);

			if (deletedCompanyVat != null)
			{
				this.viewModels.RemoveAll(x => x.Id == deletedCompanyVat.Id);
			}

			this.isInProgess = false;
		}

		protected void OnNo()
		{
			this.selectedViewModel = new CompanyVatViewModel();
		}

		public void Dispose()
		{
		}
	}
}