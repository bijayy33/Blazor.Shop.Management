﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace Blazor.Shop.Management.Pages.SymbolicPrices
{
	public class AddSymbolicPriceComponentBase : ComponentBase, IDisposable
	{
		private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

		[Inject]
		protected ShopManagementService shopService { get; set; }

		[Inject]
		protected NavigationManager navigationManager { get; set; }

		[Parameter]
		public EventCallback<MouseEventArgs> OnCancel { get; set; }

		[Parameter]
		public EventCallback<SymbolicPriceViewModel> OnAddSuccess { get; set; }

		[Parameter]
		public SymbolicPriceViewModel viewModel { get; set; }

		[Parameter]
		public List<SymbolicPriceViewModel> viewModels { get; set; }
		protected bool isSuccess { get; set; }
		protected bool submitClicked { get; set; }
		protected bool isInProgress { get; set; }

		protected override Task OnInitializedAsync()
		{
			this.isInProgress = true;
			this.viewModel = new SymbolicPriceViewModel();
			this.isInProgress = false;
			return base.OnInitializedAsync();
		}

		protected async Task OnSubmit()
		{
			if (!this.viewModels.Any(x => x.Alphabet == this.viewModel.Alphabet))
			{
				this.submitClicked = true;
				this.isInProgress = true;
				this.viewModel = await this.shopService.AddSymbolicPricesAsync(this.viewModel, cancellationTokenSource.Token);
				this.isInProgress = false;
				this.isSuccess = this.viewModel != null;

				if (this.viewModel != null)
				{
					await this.OnAddSuccess.InvokeAsync(this.viewModel);
				}
			}
		}

		protected void Cancel(MouseEventArgs args)
		{
			this.OnCancel.InvokeAsync(args);
		}

		public void Dispose()
		{
		}
	}
}
