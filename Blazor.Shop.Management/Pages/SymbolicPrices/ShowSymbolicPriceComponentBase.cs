﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Blazor.Shop.Management.Services;
using Blazor.Shop.Management.ViewModels;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.Extensions.Configuration;

namespace Blazor.Shop.Management.Pages.SymbolicPrices
{
	public class ShowSymbolicPriceComponentBase : ComponentBase, IDisposable
	{
		private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

		[Inject]
		protected ShopManagementService shopService { get; set; }

		[Inject]
		protected IConfiguration configuration { get; set; }

		[Inject]
		protected NavigationManager navigationManager { get; set; }

		[Parameter]
		public string Redirect { get; set; }
		protected SymbolicPriceViewModel selectedViewModel { get; set; }
		protected SymbolicPriceViewModel addSymbolicPrice { get; set; }
		protected bool isEdit { get; set; }
		protected bool isInProgess { get; set; }
		protected bool isSuccess { get; set; }
		protected List<SymbolicPriceViewModel> symbolicPrices { get; set; }

		protected async override Task OnInitializedAsync()
		{
			this.isInProgess = true;

			this.symbolicPrices = await this.shopService.GetSymbolicPricesAsync(cancellationTokenSource.Token);
			this.isInProgess = false;
		}

		protected void OnAdd()
		{
			this.Redirect = null;

			if (this.selectedViewModel != null)
			{
				this.symbolicPrices[this.symbolicPrices.FindIndex(x => x.Alphabet == this.selectedViewModel.Alphabet)] = this.selectedViewModel;
			}
			this.selectedViewModel = null;
			this.isEdit = false;
			this.addSymbolicPrice = new SymbolicPriceViewModel();
		}

		protected void OnAddSuccess(SymbolicPriceViewModel viewModel)
		{
			if (viewModel != null)
			{
				this.symbolicPrices.Insert(0, viewModel);
			}
		}

		protected void OnCancel(MouseEventArgs eventArgs)
		{
			this.addSymbolicPrice = null;
		}

		protected async Task OnEdit(SymbolicPriceViewModel viewModel)
		{
			await Task.Run(() =>
			{
				this.selectedViewModel = viewModel;
				this.isEdit = true;
			});
		}

		protected async Task OnDelete(SymbolicPriceViewModel viewModel)
		{
			await Task.Run(() =>
			{
				this.selectedViewModel = viewModel;
				this.isEdit = false;
			});
		}

		protected async Task OnSave(SymbolicPriceViewModel viewModel)
		{
			await this.Update(viewModel);
		}

		protected async Task OnRestore(SymbolicPriceViewModel viewModel)
		{
			viewModel.IsDeleted = false;
			await this.Update(viewModel);
		}

		protected async void OnYes(SymbolicPriceViewModel viewModel)
		{
			this.isInProgess = true;
			this.isEdit = false;
			this.isSuccess = await this.shopService.DeleteSymbolicPriceAsync(viewModel.Id, cancellationTokenSource.Token)!= null;

			if (this.isSuccess)
			{
				this.selectedViewModel = null;
			}

			this.isInProgess = false;
		}

		protected async Task OnNo()
		{
			await Task.Run(() =>
			{
				this.selectedViewModel = null;
				this.isEdit = false;
			});
		}

		public async void Dispose()
		{
		}

		private async Task Update(SymbolicPriceViewModel viewModel)
		{
			this.isInProgess = true;

			this.isSuccess = await this.shopService.UpdateSymbolicPriceAsync(viewModel, cancellationTokenSource.Token) != null;

			if (this.isSuccess)
			{
				this.selectedViewModel = null;
				this.isEdit = false;
			}

			this.isInProgess = false;
		}
	}
}
